<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['address', "client_id", "city_id"];

    public function client()
    {
        return $this->belongsTo(Partner::class,'client_id');
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
