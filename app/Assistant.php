<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assistant extends Model
{
    protected $fillable = ["assistant_id", "truck_id", "from", "to"];

    public function assistant()
    {
        return $this->belongsTo(Staff::class,'assistant_id');
    }

    public function truck()
    {
        return $this->belongsTo(Truck::class);
    }
}
