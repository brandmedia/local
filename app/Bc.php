<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Bc extends Model
{
    protected $fillable = ["nbr", 'tva_gaz', "tva_consign", "trade_id"];
    private $ht = 0;
    private $tva = 0;
    private $ttc = 0;
    public function trade()
    {
        return $this->belongsTo(Trade::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    // TODO:: delete this method in production
    public function onCreate(array $data)
    {
        $provider = Partner::find($data['provider']);
        // create new Trade
        $trade = auth()->user()->trades()->create([
            "trade"             => 2,
            "ht"                => $this->ht,
            "tva"               => $this->tva,
            "ttc"               => $this->ttc,
            "partner_id"        => $provider->id,
            "intermediate_id"   => $data['intermediate'],
            "truck_id"          => $data['transporter']
        ]);
        // create new BC
        $bc = $trade->bc()->create([
            'nbr' => $data['nbr']
        ]);
        // Order :
        foreach ($data['consign'] as $key => $qt) {
            if (!is_null($qt)) {
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;
                // create orders.
                $product->orders()->create([
                    'qt' => $qt,
                    "ht" => $ht,
                    "tva" => $tva,
                    "ttc" => $ttc,
                    "bc_id" => $bc->id,
                    "trade_id" => $trade->id,
                ]);
                // update stock provider.
                $store = $product->stock()->where('store_id', 1)->first();
                $store->update([
                    'qt' => $store->qt + $qt
                ]);
            }
        }
        foreach ($data['gaz'] as $key => $qt) {
            if (!is_null($qt)) {
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;

                // create orders.
                $product->orders()->create([
                    'qt' => $qt, "ht" => $ht, "tva" => $tva, "ttc" => $ttc, "bc_id" => $bc->id,
                    "trade_id" => $trade->id,
                ]);
                // update stock provider.
                $consign = Product::where([
                    ['bottle_size', $product->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name, '_', 'fr_FR')]
                ])->first();
                $stock_provider = Stock::where([
                    ['partner_id', $provider->id], ['product_id', $consign->id]
                ])->first();
                $stock_provider->update([
                    'qt' => $stock_provider->qt + $qt
                ]);
                $store = $consign->stock()->where('store_id', 1)->first();
                $store->update([
                    'qt' => $store->qt - $qt
                ]);
            }
        }
        // update Trade
        $trade->update([
            "ht" => $this->ht, "tva" => $this->tva, "ttc" => $this->ttc,
        ]);
        // create payment
        $this->payment($data['cheque'], 1, $provider, $trade, $data['nbr_cheque']);
        $this->payment($data['transfer'], 3, $provider, $trade, $data['nbr_operation']);
        $this->payment($data['a_term'], 4, $provider, $trade);
    }
    // TODO:: delete this method in production
    private function payment($price = null, int $mode, Partner $partner, Trade $trade, $operation = null)
    {
        if (!is_null($price) && $price != '0') {
            $payment = Payment::create([
                "price" => $price,
                "nbr_operation" => $operation,
                "mode_id" => $mode,
            ]);
            $payment->trades()->attach($trade->id);
        }
    }
}
