<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $fillable = ['charge'];
    public $timestamps = false;

    public function chargeTrucks()
    {
        return $this->hasMany(ChargeTruck::class,'charge_id');
    }
}
