<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChargeStore extends Model
{
    protected $fillable = ["name", "creator_id"];

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

    public function payments()
    {
        return $this->belongsToMany(Payment::class,'charge_store_payment');
    }
}
