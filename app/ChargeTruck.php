<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChargeTruck extends Model
{
    protected $fillable = ["truck_id", "charge_id", "trade_id", "label", "creator_id"];

    public function truck()
    {
        return $this->belongsTo(Truck::class);
    }

    public function charge()
    {
        return $this->belongsTo(Charge::class);
    }
    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

    public function trade()
    {
        return $this->belongsTo(Trade::class);
    }
}
