<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $fillable = ["provider_id", "cheque_id", "transfer_id"];

    public function getTotalAttribute()
    {
        $price = 0;
        if($cheque = $this->cheque){
            $price = $price + $cheque->price;
        }
        if($transfer = $this->transfer){
            $price = $price + $transfer->price;
        }
        return $price;
    }

    public function provider()
    {
        return $this->belongsTo(Partner::class,"provider_id");
    }

    public function cheque()
    {
        return $this->belongsTo(Payment::class);
    }

    public function transfer()
    {
        return $this->belongsTo(Payment::class);
    }

    public function claimDetails()
    {
        return $this->hasMany(ClaimDetail::class);
    }
}
