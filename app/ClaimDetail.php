<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimDetail extends Model
{
    protected $fillable = ["trade_id", "term", "inv", "claim_id", "bc_nbr"];

    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    public function trade()
    {
        return $this->belongsTo(Trade::class);
    }

    public function bc()
    {
        return $this->belongsTo(Bc::class);
    }
}
