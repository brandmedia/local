<?php

namespace App\Http\Controllers\Client;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\ClientRequest;
use App\Partner;
use App\Product;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function links()
    {
        return view('client.links');
    }

    public function all()
    {
        $clients = Partner::whereProvider('0')->get();
        return view('client.all',compact('clients'));
    }

    public function index()
    {
        $clients = Partner::whereProvider('0')->paginate(100);
        $cities = City::all();
        return view('client.index',compact('clients','cities'));
    }

    public function create()
    {
        $cities = City::all();
        return view('client.create',compact('cities'));
    }

    public function store(ClientRequest $request,Partner $client)
    {
        // prepare Data Client => creator_id && address && Discount
        $client->onCreateClient($request->all([
            "provider", "account", "name", "speaker", "rc", "patent", "ice", "address", "city"
        ]));
        session()->flash('success', "Un nouveau Client a bien été Ajouté");
        return redirect()->route('client.show',compact('client'));
    }

    public function show(Partner $client)
    {
        return view('client.show',compact('client'));
    }

    public function edit(Partner $client)
    {
        $cities = City::all();
        return view('client.edit',compact('client','cities'));
    }

    public function update(ClientRequest $request, Partner $client)
    {
        $client->address->update([
            'address' => $request->address,
            'city_id'   => $request->city
        ]);
        $client->update($request->all(["name", "speaker", "rc", "patent", "ice"]));
        session()->flash('success', "Les Informations de Votre Client Ont été mis a jour");
        return redirect()->route('client.show',compact('client'));
    }

    public function searchCity(Request $request)
    {
        // todo:: request filter
        $city = City::find($request->city);
        $addresses = $city->addresses()->with(['client'])->get();
        $clients = [];
        if(isset($addresses[0])){
            foreach ($addresses as $address) {
                $clients[] = $address->client;
            }
        }
        $cities = City::all();
        $citie = $request->city;
        return view('client.index',compact('clients','cities','citie'));
    }

    public function searchName(Request $request)
    {
        $clients = Partner::where([
            ['provider', '0'],
            ['name', 'like', '%' . $request->name . '%']
        ])->get();
        $cities = City::all();
        $name = $request->name;
        return view('client.index',compact('clients','cities','name'));
    }
}
