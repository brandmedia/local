<?php

namespace App\Http\Controllers\Intermediate;

use App\Http\Controllers\Controller;
use App\Http\Requests\Intermediate\IntermediateRequest;
use App\Intermediate;

class IntermediateController extends Controller
{
    public function index()
    {
        $intermediates = Intermediate::all();
        return view('intermediate.index',compact('intermediates'));
    }

    public function create()
    {
        return view('intermediate.create');
    }

    public function store(IntermediateRequest $request)
    {
        Intermediate::create($request->all(['name']));
        session()->flash('success', "Un nouveau Intermédiaire a bien été Ajouté");
        return redirect()->route('intermediate.index');
    }

    public function edit(Intermediate $intermediate)
    {
        return view('intermediate.edit',compact('intermediate'));
    }

    public function update(IntermediateRequest $request,Intermediate $intermediate)
    {
        $intermediate->update($request->all(['name']));
        session()->flash('success', "Le Nom de L'intermédiare a bien été mis à jour");
        return redirect()->route('intermediate.index');
    }
}
