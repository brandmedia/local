<?php

namespace App\Http\Controllers\Payment;

use App\Bc;
use App\Claim;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\ClaimRequest;
use App\Partner;
use App\Payment;
use App\Trade;
use Illuminate\Http\Request;

class ClaimController extends Controller
{
    private $trades = [];

    public function index(Partner $provider)
    {
        $claims = $provider->claims;
        return view("payment.claim.index",compact("claims","provider"));
    }

    public function create(Partner $provider)
    {
        // get trade has term
        if ($provider->provider === 1) {
            $trades = [];
            foreach ($provider->trades as $trade) {
                if ($term = $trade->payments()->where('mode_id',4)->first()) {
                    $trades[$trade->id]['trade'] = $trade;
                    $trades[$trade->id]['bc'] = $trade->bc->nbr;
                    $trades[$trade->id]['term'] = $term;
                }
            }
            return view("payment.claim",compact("provider", "trades"));
        }
        return abort(404);
    }

    public function store(Request $request,Partner $provider)
    {
        // Validators
        $valide = null;
        foreach ($request->term as $key => $term) {
            if (!is_null($term)) {
                $this->trades[] = $key;
                $valide = 1;
            }
        }
        if (is_null($valide)) {
            return back()->withErrors([
                'term' => "Votre Paiement Créance doit avoir un Prix ou plus"
            ])->withInput();
        }
        $this->add($request,$provider);
        session()->flash('success', 'Le Paiement Créance A bien été ajouté');
        return redirect()->route('provider.links');
    }

    private function add($request,Partner $provider)
    {
        $cheque = null;
        $transfer = null;
        // payments
        if($request->transfer){
            $transfer = Payment::create([
                "price"             => $request->transfer,
                "nbr_operation"     => $request->nbr_operation,
                "mode_id"           => 3
            ]);
            foreach ($this->trades as $trade) {
                $transfer->trades()->attach($trade);
            }
            $transfer = $transfer->id;
        }
        if($request->cheque){
            $cheque = Payment::create([
                "price"             => $request->cheque,
                "nbr_operation"     => $request->nbr_cheque,
                "mode_id"           => 1
            ]);
            foreach ($this->trades as $trade) {
                $cheque->trades()->attach($trade);
            }
            $cheque = $cheque->id;
        }
        // Claim
        $claim = Claim::create([
            "provider_id"   => $provider->id,
            "cheque_id"     => $cheque,
            "transfer_id"   => $transfer
        ]);
        // term
        foreach ($request->term as $key => $term) {
            if(!is_null($term)){
                $trade = Trade::find($key);
                $details = [
                    "bc_nbr"    => $trade->bc->nbr,
                    "trade_id"  => $trade->id,
                    "term"      => $term
                ];
                $t = $trade->payments()->where('mode_id',4)->first();
                $price = $t->price - $request->term[$key];
                if($price > 0 ) {
                    $t->update([
                        'price'  => $price,
                    ]);
                }
                else {
                    $t->trades()->detach($trade->id);
                    $t->delete();
                }
                if (isset($request->inv[$key]) && !is_null($request->inv[$key])) {
                    $trade->update([
                        'nbr_inv' => $request->inv[$key]
                    ]);
                    $details['inv'] = $request->inv[$key];
                    $claim->claimDetails()->create($details);
                }
            }
        }
    }

    private function sub(Claim $claim)
    {
        // delete Payment
        if ($cheque = $claim->cheque) {
            $cheque->trades()->detach($claim->trade_id);
            $cheque->delete();
        }
        if ($transfer = $claim->transfer) {
            $transfer->trades()->detach($claim->trade_id);
            $transfer->delete();
        }
        // sub details
        foreach ($claim->claimDetails as $detail) {
            // sub term
            if ($term = $detail->trade->payments()->where('mode_id',4)->first()) {
                $term->update([
                    'price'     => $term->price + $detail->term
                ]);
            }
            else {
                $payment = Payment::create([
                    "price"     => $detail->term,
                    "mode_id"   => 4
                ]);
                $payment->trades()->attach($detail->trade_id);
            }
            if ($detail->inv) {
                $detail->trade->update([
                    "nbr_inv"   => null
                ]);
            }
            $detail->delete();
        }
        // delete Claim
        $claim->delete();
    }

    public function destroy(Partner $provider, Claim $claim)
    {
        $this->sub($claim);
        session()->flash('success', "Le Paiement Créance a bien été Supprimé");
        return redirect()->route("provider.index",compact("provider"));
    }

}
