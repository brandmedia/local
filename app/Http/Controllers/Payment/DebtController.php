<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\DebtRequest;
use App\Partner;
use App\Payment;
use App\Trade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DebtController extends Controller
{
    private $cheque = 0;
    private $transfer = 0;
    private $trades = [];

    public function create()
    {
        return view("payment.debt");
    }

    public function store(DebtRequest $request)
    {
        // Validators
        $valide = 0;
        foreach ($request->inv as $key => $inv) {
            if (!is_null($inv)) {
                $valide = 1;
                if (is_null($request->price[$key]) || empty($request->price[$key])){
                    return back()->withErrors([
                        'price.' . $key => "Tous les Factures Mentionner doivent avoir un montant"
                    ])->withInput();
                }
            }
        }
        if($valide == 0){
            return back()->withErrors([
                'inv'=> "Votre Encaissement Créance doit avoir une facture ou plus"
            ])->withInput();
        }

        // inv
        foreach ($request->inv as $key => $nbr) {
            if(!is_null($nbr)){
                $trade = Trade::where("nbr_inv",$nbr)->first();
                $this->trades[] = $trade->id;
                $term = $trade->payments()->where('mode_id',4)->first();
                $price = $term->price - $request->price[$key];
                if($price > 0 ){
                    $term->update([
                        'price'  => $price,
                    ]);
                }
                else{
                    $term->delete();
                }
            }
        }

        // payments
        if($request->transfer){
            $this->transfer = $request->transfer;
            $t = Payment::create([
                "price"             => $request->transfer,
                "nbr_operation"     => $request->nbr_operation,
                "mode_id"           => 3
            ]);
            foreach ($this->trades as $trade) {
                $t->trades()->attach($trade);
            }
        }
        if($request->cheque){
            $this->transfer = $request->cheque;
            $t = Payment::create([
                "price"             => $request->cheque,
                "nbr_operation"     => $request->nbr_cheque,
                "mode_id"           => 1
            ]);
            foreach ($this->trades as $trade) {
                $t->trades()->attach($trade);
            }
        }
        session()->flash('success', "L'encaissement Créance A bien été ajouté");
        return redirect()->route('saisie');
    }

}
