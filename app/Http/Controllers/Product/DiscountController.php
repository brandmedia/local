<?php

namespace App\Http\Controllers\Product;

use App\Discount;
use App\Http\Controllers\Controller;
use App\Partner;
use App\Product;
use Illuminate\Http\Request;

class DiscountController extends Controller
{

    public function create(Partner $client)
    {
        if ($client->provider != 0){
            return abort(404);
        }
        $gazes = Product::where('name', 'Gaz')->get();
        return view("trade.discount.create",compact("client","gazes"));
    }

    public function store(Request $request,Partner $client)
    {
        foreach ($request->gazes as $key => $gaze) {
            $discount = Discount::where([
                ['product_id', $key],
                ['partner_id', $client->id]
            ])->first();
            $discount->update([
                'price'     => $gaze
            ]);
        }
        return redirect()->route('discount.create',compact('client'));
    }

}
