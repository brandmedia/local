<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Partner;
use App\Price;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PricesController extends Controller
{

    public function create()
    {
        $providers = Partner::where('provider', 1)->get();
        $consignees = [];
        foreach ($providers as $provider) {
            $consignees[$provider->id] = Product::where('name', Str::slug('consign_' . $provider->name, '_', 'fr_FR'))->get();
        }
        return view('trade.price.create',compact('consignees'));
    }

    public function store(Request $request)
    {
        foreach ($request->consign_buy as $key => $consign) {
            Price::create([
                'buy'           => $consign,
                'sale'          => $request->consign_sale[$key],
                'product_id'    => $key,
                "creator_id"    => auth()->id()
            ]);
        }
        session()->flash('success', "Les Prix ont bien été mis a jour");
        return redirect()->route('saisie');
    }

}
