<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\ProviderRequest;
use App\Partner;
use App\Price;
use App\Product;
use Illuminate\Http\Request;

class ProviderController extends Controller
{

    public function links()
    {
        return view('provider.links');
    }

    public function index()
    {
        $providers = Partner::whereProvider('1')
            ->orWhere('provider',3)
            ->get();
        return view('provider.index',compact('providers'));
    }

    public function create()
    {
        return view('provider.create');
    }

    public function store(ProviderRequest $request,Product $product,Partner $partner)
    {
        $partner->onCreateProvider($request->all([
            "provider", "account", "name", "speaker", "rc", "patent", "ice"
        ]),$product);
        // TODO:: notification prices products
        session()->flash('success', "Un nouveau Fournisseur a bien été Ajouté");
        return redirect()->route('provider.index');
    }

    public function show(Partner $provider,Price $price)
    {
        $products = [];
        if(isset($provider->stock[0])){
            $products["total"] = 0;
            foreach ($provider->stock as $stock) {
                $consign = $stock->product;
                $ttc = $price->getPriceRemplie($consign,$stock);
                $qt = $stock->qt;
                $size = $consign->bottle_size;
                $products[$size] = ['ttc' => $ttc, 'qt' => $qt];
                $products['total'] = $products['total'] + $ttc;
            }
        }
        // todo:: list of trades
        return view('provider.show',compact('provider','products'));
    }

    public function edit(Partner $provider)
    {
        return view('provider.edit', compact('provider'));
    }

    public function update(ProviderRequest $request,Partner $provider,Product $product)
    {
        if ($request->name != $provider->name){
            $product->onUpdateProvider($provider,$request->name);
        }
        $provider->update($request->all(["name", "speaker", "rc", "patent", "ice"]));
        session()->flash('success', "Les informations du Fournisseur ont bien été mis à jour");
        return redirect()->route('provider.show',compact('provider'));
    }

}
