<?php

namespace App\Http\Controllers\Trade;

use App\Bc;
use App\Http\Controllers\Controller;
use App\Http\Requests\Trade\BcRequest;
use App\Intermediate;
use App\Order;
use App\Partner;
use App\Payment;
use App\Product;
use App\Stock;
use App\Trade;
use App\Truck;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BcController extends Controller
{
    private $ht = 0;
    private $tva = 0;
    private $ttc = 0;

    public function index()
    {
        //
    }

    public function create()
    {
        $providers = Partner::where('provider', 1)->get();
        $intermediates = Intermediate::all();
        $transporters = Truck::where('transporter', 1)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        $consignees = [];
        foreach ($providers as $provider) {
            $consignees[$provider->id] = Product::where('name', Str::slug('consign_' . $provider->name, '_', 'fr_FR'))->get();
        }
        return view('trade.bc.create', compact('providers', 'intermediates', 'transporters', 'gazes', 'consignees'));
    }

    public function store(BcRequest $request)
    {
        $valide = null;
        foreach ($request->gaz as $gaze) {
            if (!is_null($gaze) && $gaze != '0') {
                $valide = 1;
            }
        }
        if (is_null($valide)) {
            return back()->withErrors([
                'gaz' => "votre Bon de Commande doit avoir un produit de GAZ ou plus"
            ])->withInput();
        }
        // create new Trade

        $provider = Partner::find($request->provider);
        $trade = auth()->user()->trades()->create([
            "trade"             => 2,
            "ht"                => $this->ht,
            "tva"               => $this->tva,
            "ttc"               => $this->ttc,
            "partner_id"        => $provider->id,
            "intermediate_id"   => $request->intermediate,
            "truck_id"          => $request->transporter
        ]);
        // create new BC
        $bc = $trade->bc()->create([
            'nbr' => $request->nbr
        ]);
        // Order :
        foreach ($request->consign as $key => $qt) {
            if (!is_null($qt)) {
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;
                // create orders.
                $product->orders()->create([
                    'qt' => $qt,
                    "ht" => $ht,
                    "tva" => $tva,
                    "ttc" => $ttc,
                    "bc_id" => $bc->id,
                    "trade_id" => $trade->id,
                ]);
                // update stock provider.
                $store = $product->stock()->where('store_id', 1)->first();
                $store->update([
                    'qt' => $store->qt + $qt
                ]);
            }
        }
        foreach ($request->gaz as $key => $qt) {
            if (!is_null($qt)) {
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;

                // create orders.
                $product->orders()->create([
                    'qt' => $qt, "ht" => $ht, "tva" => $tva, "ttc" => $ttc, "bc_id" => $bc->id,
                    "trade_id" => $trade->id,
                ]);
                // update stock provider.
                $consign = Product::where([
                    ['bottle_size', $product->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name, '_', 'fr_FR')]
                ])->first();
                $stock_provider = Stock::where([
                    ['partner_id', $provider->id], ['product_id', $consign->id]
                ])->first();
                $stock_provider->update([
                    'qt' => $stock_provider->qt + $qt
                ]);
                $store = $consign->stock()->where('store_id', 1)->first();
                $store->update([
                    'qt' => $store->qt - $qt
                ]);
            }
        }
        // update Trade
        $trade->update([
            "ht" => $this->ht, "tva" => $this->tva, "ttc" => $this->ttc,
        ]);
        // create payment
        $this->payment($request->cheque, 1, $provider, $trade, $request->nbr_cheque);
        $this->payment($request->transfer, 3, $provider, $trade, $request->nbr_operation);
        $this->payment($request->a_term, 4, $provider, $trade);
        session()->flash('success', 'Un nouveau bon de commande a bien été ajouté');
        return redirect()->route('provider.links');
    }

    public function show(Bc $bc)
    {
        dd($bc);
    }

    public function edit(Bc $bc)
    {
        $providers = Partner::where('provider', 1)->get();
        $intermediates = Intermediate::all();
        $transporters = Truck::where('transporter', 1)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        $consignees = [];
        foreach ($providers as $provider) {
            $products = Product::where(
                'name', Str::slug('consign_' . $provider->name, '_', 'fr_FR')
            )->get();
            $consignees[$provider->id] = $products;
        }
        $cheque = null;
        $nbr_cheque = null;
        if ($pay = $bc->trade->payments()->where('mode_id', 1)->first()) {
            $cheque = $pay->price;
            $nbr_cheque = $pay->nbr_operation;
        }
        $transfer = null;
        $nbr_operation = null;
        if ($pay = $bc->trade->payments()->where('mode_id', 3)->first()) {
            $transfer = $pay->price;
            $nbr_operation = $pay->nbr_operation;
        }
        $a_term = null;
        if ($pay = $bc->trade->payments()->where('mode_id', 4)->first()) {
            $a_term = $pay->price;
        }
        return view('trade.bc.edit', compact('bc', 'providers', 'intermediates', 'transporters', 'gazes', 'consignees', 'a_term', 'nbr_operation', 'nbr_cheque', 'cheque', 'transfer'));
    }

    public function update(Request $request, Bc $bc)
    {
        // create new Trade
        $old_provider = $bc->trade->partner;
        $provider = Partner::find($request->provider);
        $trade = $bc->trade;
        $trade->update([
            "ht" => $this->ht, "tva" => $this->tva, "ttc" => $this->ttc, "partner_id" => $provider->id,
            "intermediate_id" => $request->intermediate, "truck_id" => $request->transporter
        ]);
        // bc
        $bc->update([
            'nbr' => $request->nbr
        ]);
        // Order :
        // delete orders
        foreach ($bc->orders as $order) {
            $this->updateStock($order, $old_provider);
            $order->delete();
        }
        // create Orders
        foreach ($request->consign as $key => $qt) {
            if (!is_null($qt)) {
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;
                // create orders.
                $product->orders()->create([
                    "qt" => $qt, "ht" => $ht, "tva" => $tva, "ttc" => $ttc, "bc_id" => $bc->id,
                    "trade_id" => $trade->id,
                ]);
                // update stock provider.
                $store = $product->stock()->where('store_id', 1)->first();
                $store->update([
                    'qt' => $store->qt + $qt
                ]);
            }
        }
        foreach ($request->gaz as $key => $qt) {
            if (!is_null($qt)) {
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;
                // create orders.
                $product->orders()->create([
                    "qt" => $qt, "ht" => $ht,
                    "tva" => $tva, "ttc" => $ttc,
                    "bc_id" => $bc->id,
                    "trade_id" => $trade->id,
                ]);
                // update stock provider.
                $consign = Product::where([
                    ['bottle_size', $product->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name, '_', 'fr_FR')]
                ])->first();
                $stock_provider = Stock::where([
                    ['partner_id', $provider->id], ['product_id', $consign->id]
                ])->first();
                $stock_provider->update([
                    'qt' => $stock_provider->qt + $qt
                ]);
                $store = $consign->stock()->where('store_id', 1)->first();
                $store->update([
                    'qt' => $store->qt - $qt
                ]);
            }
        }

        // update Trade
        $trade->update([
            "ht" => $this->ht, "tva" => $this->tva, "ttc" => $this->ttc,
        ]);
        // payment
        // delete payments
        foreach ($trade->payments as $payment) {
            $payment->delete();
        }
        // create payments
        $this->payment($request->cheque, 1, $provider, $trade, $request->nbr_cheque);
        $this->payment($request->transfer, 3, $provider, $trade, $request->nbr_operation);
        $this->payment($request->a_term, 4, $provider, $trade);
        session()->flash('success', 'Votre bon de commande a bien été mis a jour');
        return redirect()->route('provider.links');
    }

    private function payment($price = null, int $mode, Partner $partner, Trade $trade, $operation = null)
    {
        if (!is_null($price) && $price != '0') {
            $payment = Payment::create([
                "price" => $price, "nbr_operation" => $operation, "mode_id" => $mode, "partner_id" => $partner->id
            ]);
            $payment->trades()->attach($trade->id);
        }
    }

    private function updateStock(Order $order, Partner $partner)
    {
        if ($order->product->name == 'Gaz') {
            $consign = Product::where([
                ['bottle_size', $order->product->bottle_size],
                ['name', Str::slug('consign_' . $partner->name, '_', 'fr_FR')]
            ])->first();
            $stock_provider = Stock::where([
                ['partner_id', $partner->id], ['product_id', $consign->id]
            ])->first();
            $stock_provider->update([
                'qt' => $stock_provider->qt - $order->qt
            ]);
            $store = $consign->stock()->where('store_id', 1)->first();
            $store->update([
                'qt' => $store->qt + $order->qt
            ]);
        }
        if ($order->product->name == Str::slug('consign_' . $partner->name, '_', 'fr_FR')) {
            $consign = Product::where([
                ['bottle_size', $order->product->bottle_size],
                ['name', Str::slug('consign_' . $partner->name, '_', 'fr_FR')]
            ])->first();
            $store = $consign->stock()->where('store_id', 1)->first();
            $store->update([
                'qt' => $store->qt - $order->qt
            ]);
        }
    }
}
