<?php

namespace App\Http\Controllers\Trade;

use App\Bl;
use App\Http\Controllers\Controller;
use App\Http\Requests\Trade\BlRequest;
use App\Intermediate;
use App\Partner;
use App\Product;
use App\Stock;
use App\Truck;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlController extends Controller
{
    private $ht = 0;
    private $tva = 0;
    private $ttc = 0;

    public function create()
    {
        $providers = Partner::where('provider', 1)->get();
        $intermediates = Intermediate::all();
        $transporters = Truck::where('transporter', 1)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        return view('trade.bl.create',
            compact('providers', 'intermediates', 'transporters', 'gazes')
        );
    }

    public function store(BlRequest $request)
    {
        // create new Trade
        $provider = Partner::find($request->provider);
        $trade = auth()->user()->trades()->create([
            "trade"             => 3,
            "ht"                => $this->ht,
            "tva"               => $this->tva,
            "ttc"               => $this->ttc,
            "partner_id"        => $provider->id,
            "intermediate_id"   => $request->intermediate,
            "truck_id"          => $request->transporter
        ]);
        // create new BL
        $bl = $trade->bl()->create([
            'nbr'   => $request->nbr
        ]);
        // Order :
        foreach ($request->gaz as $key => $qt) {
            if(!is_null($qt)){
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;

                // create orders.
                $product->orders()->create([
                    "qt"            => $qt,
                    "ht"            => $ht,
                    "tva"           => $tva,
                    "ttc"           => $ttc,
                    "bl_id"         => $bl->id,
                    "trade_id"      => $trade->id,
                ]);
                // update stock provider.
                $consign = Product::where([
                    ['bottle_size',$product->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name,'_','fr_FR')]
                ])->first();
                $stock_provider = Stock::where([
                    ['partner_id',$provider->id],
                    ['product_id',$consign->id]
                ])->first();
                $stock_provider->update([
                    'qt'    => $stock_provider->qt - $qt
                ]);
                // update stock store
                $store = $consign->stock()->where('store_id',1)->first();
                $store->update([
                    'qt'    => $store->qt + $qt
                ]);
                $store = $product->stock()->where('store_id',1)->first();
                $store->update([
                    'qt'    => $store->qt + $qt
                ]);
            }
        }
        // update Trade
        $trade->update([
            "ht"        => $this->ht,
            "tva"       => $this->tva,
            "ttc"       => $this->ttc,
        ]);
        session()->flash('success','Un nouveau bon de Livraison a bien été ajouté');
        return redirect()->route('provider.links');
    }

    public function edit(Bl $bl)
    {
        $providers = Partner::where('provider', 1)->get();
        $intermediates = Intermediate::all();
        $transporters = Truck::where('transporter', 1)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        return view('trade.bl.edit',
            compact('providers', 'intermediates', 'transporters', 'gazes','bl')
        );
    }

    public function update(Request $request, Bl $bl)
    {
        $trade = $bl->trade;
        $old_provider = Partner::find($trade->partner_id);
        // delete orders
        $this->updateStore($bl,$old_provider);
        // update Trade
        $provider = Partner::find($request->provider);
        $trade->update([
            "ht"                => $this->ht,
            "tva"               => $this->tva,
            "ttc"               => $this->ttc,
            "partner_id"        => $provider->id,
            "intermediate_id"   => $request->intermediate,
            "truck_id"          => $request->transporter
        ]);
        // bl
        $bl->update([
            'nbr'   => $request->nbr
        ]);
        // Order :
        foreach ($request->gaz as $key => $qt) {
            if(!is_null($qt)){
                $product = Product::find($key);
                $price = $product->lastPrice()['buy'];
                $ht = $qt * $price;
                $this->ht = $this->ht + $ht;
                $tva = $ht * $product->tva / 100;
                $this->tva = $this->tva + $tva;
                $ttc = $ht + $tva;
                $this->ttc = $this->ttc + $ttc;

                // create orders.
                $product->orders()->create([
                    "qt"            => $qt,
                    "ht"            => $ht,
                    "tva"           => $tva,
                    "ttc"           => $ttc,
                    "bl_id"         => $bl->id,
                    "trade_id"      => $trade->id,
                ]);
                // update stock provider.
                $consign = Product::where([
                    ['bottle_size',$product->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name,'_','fr_FR')]
                ])->first();
                $stock_provider = Stock::where([
                    ['partner_id',$provider->id],
                    ['product_id',$consign->id]
                ])->first();
                $stock_provider->update([
                    'qt'    => $stock_provider->qt - $qt
                ]);
                $store_consign = $consign->stock()->where('store_id',1)->first();
                $store_consign->update([
                    'qt'    => $store_consign->qt + $qt
                ]);
                $store_gaz = $product->stock()->where('store_id',1)->first();
                $store_gaz->update([
                    'qt'    => $store_gaz->qt + $qt
                ]);
            }
        }
        // update Trade
        $trade->update([
            "ht"        => $this->ht,
            "tva"       => $this->tva,
            "ttc"       => $this->ttc,
        ]);

        session()->flash('success','Votre bon de livraison a bien été mis a jour');
        return redirect()->route('provider.links');
    }

    private function updateStore(Bl $bl, Partner $p)
    {
        foreach ($bl->orders as $order){
            $product = $order->product;
            $consign = Product::where([
                ['bottle_size',$product->bottle_size],
                ['name', Str::slug('consign_' . $p->name,'_','fr_FR')]
            ])->first();
            $stock_provider = Stock::where([
                ['partner_id',$p->id],
                ['product_id',$consign->id]
            ])->first();
            $store_consign = $consign->stock()->where('store_id',1)->first();
            $store_gaz = $product->stock()->where('store_id',1)->first();
            $stock_provider->update([
                'qt'    => $stock_provider->qt + $order->qt
            ]);
            $store_consign->update([
                'qt'    => $store_consign->qt - $order->qt
            ]);
            $store_gaz->update([
                'qt'    => $store_gaz->qt - $order->qt
            ]);
            $order->delete();
        }
    }
}
