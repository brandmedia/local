<?php

namespace App\Http\Controllers\Trade;

use App\Bc;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\PaymentRequest;
use App\Partner;
use App\Payment;

class InvController extends Controller
{
    private $cheque = 0;
    private $transfer = 0;
    private $trades = [];

    public function create()
    {
        $providers = Partner::where('provider', 1)->get();
        return view("trade.inv.create",compact('providers'));
    }

    public function store(PaymentRequest $request)
    {
        // Validators
        $valide = null;
        $price_bc = true;
        foreach ($request->bc as $key => $nbr) {
            if (!is_null($nbr)) {
                $valide = 1;
                if (is_null($request->price[$key]) || empty($request->price[$key])){
                    $price_bc = null;
                }
            }
        }

        if (is_null($valide)) {
            return back()->withErrors([
                'bc' => "Votre Paiement Créance doit avoir un bon de commande ou plus"
            ])->withInput();
        }

        if (is_null($price_bc)) {
            return back()->withErrors([
                'price' => "Tous les Bon de commande Mentionner doivent avoir un montant"
            ])->withInput();
        }

        // bc
        foreach ($request->bc as $key => $nbr) {
            if(!is_null($nbr)){
                $bc = Bc::where('nbr',$nbr)->first();
                $this->trades[] = $bc->trade_id;
                $trade = $bc->trade;
                $term = $trade->payments()->where('mode_id',4)->first();
                $price = $term->price - $request->price[$key];
                $nbr_inv = $trade->inv;
                if($request->inv){
                    $nbr_inv  = $request->inv;
                }
                $term->update([
                    'price'  => $price,
                    'nbr_inv' => $nbr_inv
                ]);
            }
        }
        // payments
        if($request->transfer){
            $this->transfer = $request->transfer;
            $t = Payment::create([
                "price"             => $request->transfer,
                "nbr_operation"     => $request->nbr_operation,
                "mode_id"           => 3
            ]);
            foreach ($this->trades as $trade) {
                $t->trades()->attach($trade);
           }
        }

        if($request->cheque){
            $this->transfer = $request->cheque;
            $t = Payment::create([
                "price"             => $request->cheque,
                "nbr_operation"     => $request->nbr_cheque,
                "mode_id"           => 1
            ]);
            foreach ($this->trades as $trade) {
                $t->trades()->attach($trade);
            }
        }

        session()->flash('success', 'Le Paiement Créance A bien été ajouté');
        return redirect()->route('saisie');
    }

}
