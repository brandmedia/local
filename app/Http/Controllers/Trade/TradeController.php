<?php

namespace App\Http\Controllers\Trade;

use App\Http\Controllers\Controller;
use App\Invoice;
use App\Partner;
use App\Payment;
use App\Product;
use App\Trade;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TradeController extends Controller
{
    private $ht = 0;
    private $tva = 0;
    private $ttc = 0;
    private $trades = [];
    // todo:: Filtration data with request
    public function create()
    {
        $gazes = Product::where('name', 'Gaz')->get();
        $providers = Partner::where('provider', 1)->get();
        $consignees = [];
        foreach ($providers as $provider) {
            $consignees[$provider->id] = Product::where('name', Str::slug('consign_' . $provider->name, '_', 'fr_FR'))->get();
        }
        return view('trade.trade',compact('gazes','providers','consignees'));
    }

    public function store(Request $request, Trade $trade)
    {
        // validator
        $valide = null;
        $buy = null;
        $sale = null;
        foreach ($request->gaz as $gaze) {
            if (!is_null($gaze) && $gaze != '0') {
                $valide = 1;
                $sale = 1;
            }
        }
        foreach ($request->consign as $consign) {
            if (!is_null($consign) && $consign != '0') {
                $valide = 1;
                $sale = 1;
            }
        }
        foreach ($request->consign_buy as $consign) {
            if (!is_null($consign) && $consign != '0') {
                $valide = 1;
                $buy = 1;
            }
        }
        if (is_null($valide)) {
            return back()->withErrors([
                'gaz' => "Votre Vente Achat est Vide Veuillez indiquez des produits"
            ])->withInput();
        }
        $provider = Partner::find(1);
        // sale
        if($sale){
            $nbr_inv = $trade->getSaleIncrements();
            $trade = $trade->create([
                "trade"             => 1,
                "slug_inv"          => $nbr_inv,
                "nbr_inv"           => str_replace('-','',$nbr_inv),
                "ht"                => $this->ht,
                "tva"               => $this->tva,
                "ttc"               => $this->ttc,
                "partner_id"        => $provider->id,
                'creator_id'        => auth()->id()
            ]);
            $this->trades[] = $trade->id;
            foreach ($request->consign as $key => $qt) {
                if (!is_null($qt)) {
                    $product = Product::find($key);
                    $price = $product->lastPrice()['sale'];
                    $ht = $qt * $price;
                    $this->ht = $this->ht + $ht;
                    $tva = $ht * $product->tva / 100;
                    $this->tva = $this->tva + $tva;
                    $ttc = $ht + $tva;
                    $this->ttc = $this->ttc + $ttc;
                    // create orders.
                    $product->orders()->create([
                        'qt'        => $qt,
                        "ht"        => $ht,
                        "tva"       => $tva,
                        "ttc"       => $ttc,
                        "trade_id"  => $trade->id,
                    ]);
                    // update stock provider.
                    $store = $product->stock()->where('store_id', 1)->first();
                    $store->update([
                        'qt' => $store->qt - $qt
                    ]);
                }
            }
            foreach ($request->gaz as $key => $qt) {
                if (!is_null($qt)) {
                    $product = Product::find($key);
                    $price = $product->lastPrice()['sale'];
                    $ht = $qt * $price;
                    $this->ht = $this->ht + $ht;
                    $tva = $ht * $product->tva / 100;
                    $this->tva = $this->tva + $tva;
                    $ttc = $ht + $tva;
                    $this->ttc = $this->ttc + $ttc;
                    // create orders.
                    $product->orders()->create([
                        'qt' => $qt,
                        "ht" => $ht,
                        "tva" => $tva,
                        "ttc" => $ttc,
                        "trade_id" => $trade->id,
                    ]);
                    // update stock provider.
                    $store = $product->stock()->where('store_id', 1)->first();
                    $store->update([
                        'qt' => $store->qt - $qt
                    ]);
                }
            }
            // invoice
            $invoice = new Invoice();
            $invoice->invoice($trade);
            $trade->update([
                "ht"    => $this->ht,
                "tva"   => $this->tva,
                "ttc"   => $this->ttc,
            ]);
        }
        // buy
        if($buy){
            $this->ht = 0;
            $this->tva = 0;
            $this->ttc = 0;
            $trade = $trade->create([
                "trade"             => 0,
                "ht"                => $this->ht,
                "tva"               => $this->tva,
                "ttc"               => $this->ttc,
                "partner_id"        => $provider->id,
                'creator_id'        => auth()->id()
            ]);
            $this->trades[] = $trade->id;
            foreach ($request->consign_buy as $key => $qt) {
                if (!is_null($qt)) {
                    $product = Product::find($key);
                    $price = $product->lastPrice()['buy'];
                    $ht = $qt * $price;
                    $this->ht = $this->ht + $ht;
                    $tva = $ht * $product->tva / 100;
                    $this->tva = $this->tva + $tva;
                    $ttc = $ht - $tva;
                    $this->ttc = $this->ttc + $ttc;
                    // create orders.
                    $product->orders()->create([
                        'qt' => $qt,
                        "ht" => $ht,
                        "tva" => $tva,
                        "ttc" => $ttc,
                        "trade_id" => $trade->id,
                    ]);
                    // update stock provider.
                    $store = $product->stock()->where('store_id', 1)->first();
                    $store->update([
                        'qt' => $store->qt + $qt
                    ]);
                }
            }
            $trade->update([
                "ht"    => $this->ht,
                "tva"   => $this->tva,
                "ttc"   => $this->ttc,
            ]);
        }
        // create payment
        $this->payment($request->cheque, 1, $provider, $trade, $request->nbr_cheque);
        $this->payment($request->transfer, 3, $provider, $trade, $request->nbr_operation);
        $this->payment($request->a_term, 4, $provider, $trade);
        session()->flash('success', "Votre transaction est réussi");
        return redirect()->route('home');
    }

    private function payment($price = null, int $mode, Partner $partner, Trade $trade, $operation = null)
    {
        if (!is_null($price) && $price != '0') {
            $payment = Payment::create([
                "price" => $price,
                "nbr_operation" => $operation,
                "mode_id" => $mode,
                "partner_id" => $partner->id
            ]);
            $payment->trades()->attach($this->trades);
        }
    }

}
