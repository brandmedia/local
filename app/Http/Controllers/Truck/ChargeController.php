<?php

namespace App\Http\Controllers\Truck;

use App\Charge;
use App\ChargeTruck;
use App\Http\Controllers\Controller;
use App\Http\Requests\Truck\ChargeTruckRequest;
use App\Payment;
use App\Trade;
use App\Truck;

class ChargeController extends Controller
{

    private $trades = [];

    public function create()
    {
        $charges = Charge::all();
        $trucks  = Truck::all();
        return view('charge.create',compact('trucks',"charges"));
    }

    public function store(ChargeTruckRequest $request)
    {
        // in trade
        foreach ($request->truck as $key => $truck) {
            // add trade
            $trade = Trade::create([
                "trade"             => 4,
                "ht"                => 0,
                "tva"               => 0,
                "ttc"               => 0,
                "partner_id"        => 2,
                "creator_id"        => auth()->id(),
            ]);
            $this->trades[] = $trade->id;
            // create charge truck
            ChargeTruck::create([
                "truck_id"      => $truck,
                "charge_id"     => $request->charge[$key],
                "trade_id"      => $trade->id,
                "label"         => $request->label[$key],
                "creator_id"    => auth()->id()
            ]);
            // add payments
            if($request->transfer){
                $t = Payment::create([
                    "price"             => $request->transfer,
                    "nbr_operation"     => $request->nbr_operation,
                    "mode_id"           => 3
                ]);
                foreach ($this->trades as $trade) {
                    $t->trades()->attach($trade);
                }
            }

            if($request->cheque){
                $t = Payment::create([
                    "price"             => $request->cheque,
                    "nbr_operation"     => $request->nbr_cheque,
                    "mode_id"           => 1
                ]);
                foreach ($this->trades as $trade) {
                    $t->trades()->attach($trade);
                }
            }

            if($request->a_term){
                $t = Payment::create([
                    "price"             => $request->a_term,
                    "mode_id"           => 4
                ]);
                foreach ($this->trades as $trade) {
                    $t->trades()->attach($trade);
                }
            }

        }
        session()->flash('success',"Votre Transaction a bien été ajouté");
        return back();
    }
}
