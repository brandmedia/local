<?php

namespace App\Http\Controllers\Truck;

use App\Http\Controllers\Controller;
use App\Http\Requests\Truck\LoadingRequest;
use App\Loading;
use App\Partner;
use App\Payment;
use App\Product;
use App\Truck;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoadingController extends Controller
{
    public function index()
    {
        $loadings = Loading::all();
        return view('loading.index',compact('loadings'));
    }

    public function create()
    {
        $trucks = Truck::where('transporter', false)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        $providers = Partner::where('provider', 1)->get();
        return view('loading.create',compact('trucks','providers','gazes'));
    }

    public function store(Request $request)
    {
        // validation Gaze
        $valide = null;
        foreach ($request->gaz as $key => $qt) {
            if(!is_null($qt) && $qt != '0') {
                $valide = 1;
            }
        }
        if(is_null($valide)){
            return back()->withErrors(['gaz' => "votre Bon de Chargement doit avoir au moins un produit ou plus"])
                ->withInput();
        }
        $provider = Partner::find($request->provider);
        if (is_null($provider)) {
            return back()->withErrors(['provider' => "Veuillez Choisir une marque de produit valide"])
                ->withInput();
        }
        // payment
        $payment_id = null;
        if($request->cash){
            $payment = Payment::create([
                "price"     => $request->cash,
                "mode_id"   => 2
            ]);
            $truck = Truck::find($request->truck);
            $truck->update([
               "cash"   => $truck->cash + $request->cash
            ]);
            $payment_id = $payment->id;
        }
        // create new loading
        $loading = Loading::create([
            "truck_id"      => $request->truck,
            "creator_id"    => auth()->id(),
            'partner_id'    => $provider->id,
            'payment_id'    => $payment_id
        ]);
        // create new Tmp
        foreach ($request->gaz as $key => $qt) {
            if(!is_null($qt) && $qt != "0") {
                $product_gaz = Product::find($key);
                $product_consign = Product::where([
                    ['bottle_size',$product_gaz->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name,'_','fr_FR')]
                ])->first();
                $loading->tmps()->create([
                    "qt"            => $qt,
                    "product_id"    => $product_gaz->id
                ]);
                $loading->tmps()->create([
                    "qt"            => $qt,
                    "product_id"    => $product_consign->id
                ]);
            }
        }
        session()->flash('success', "success");
        return redirect()->route('distributeur.links');
    }

    public function edit(Loading $loading)
    {
        $trucks = Truck::where('transporter', false)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        $providers = Partner::where('provider', 1)->get();
        foreach ($gazes as $gaze) {
            if($tmp = $loading->tmps()->where('product_id',$gaze->id)->first()){
                $gaze->qt = $tmp->qt;
            }
        }
        $price = null;
        if ($payment = $loading->payment){
            $price = $payment->price;
        }
        return view('loading.edit',compact("loading", "trucks", "gazes", "providers", "price"));
    }

    public function update(Request $request, Loading $loading)
    {
        $payment = null;
        $truck = Truck::find($request->truck);
        // update payment
            // if has payment && payment not null
        if($loading->payment && !is_null($request->cash)){
            $loading->truck->update([
                "cash"  => $loading->truck - $loading->payment->price
            ]);
            $loading->payment->update(['price' => $request->cash]);
            $truck->update([
                "cash"  => $loading->payment->price + $request->cash
            ]);
        }
            // if not has payment && payment not null
        elseif(!$loading->payment && !is_null($request->cash)){
            $payment = Payment::create(['mode_id' => 2,"price" => $request->cash]);
            $loading->update([
                'payment_id'    => $payment->id
            ]);
            $truck->update([
                "cash"  => $truck->cash + $request->cash
            ]);
        }
            // if has payment && payment null
        elseif($loading->payment && is_null($request->cash)){
            $loading->truck->update([
                "cash"  => $loading->truck->cash - $loading->payment->price
            ]);
            $loading->payment->delete();
            $loading->update([
                'payment_id'    => null
            ]);
        }
        // update loading
        $provider = Partner::find($request->provider);
        $loading->update([
            "truck_id"      => $request->truck,
            'partner_id'    => $provider->id,
        ]);
        // delete tmp
        foreach ($loading->tmps as $tmp) {
            $tmp->delete();
        }
        // create tmp
        foreach ($request->gaz as $key => $qt) {
            if(!is_null($qt)) {
                $product_gaz = Product::find($key);
                $product_consign = Product::where([
                    ['bottle_size',$product_gaz->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name,'_','fr_FR')]
                ])->first();
                $loading->tmps()->create([
                    "qt"            => $qt,
                    "product_id"    => $product_gaz->id
                ]);
                $loading->tmps()->create([
                    "qt"            => $qt,
                    "product_id"    => $product_consign->id
                ]);
            }
        }
        return redirect()->route('loading.edit',compact('loading'));
    }

    // TODO :: update valides loading (touch stock).
    public function updateValide(Request $request, Loading $loading)
    {
        // delete payment
        $loading->payment->delete();
        $loading->update(['payment_id' => null]);
        // delete tmp
        foreach ($loading->tmps as $tmp) {
            // todo :: touch stock
        }
        $loading->tmps();
        // update loading
        // create tmp
    }
}
