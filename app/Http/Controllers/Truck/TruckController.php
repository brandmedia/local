<?php

namespace App\Http\Controllers\Truck;

use App\Assistant;
use App\Category;
use App\Driver;
use App\Http\Controllers\Controller;
use App\Http\Requests\Truck\TruckRequest;
use App\Product;
use App\Truck;

class TruckController extends Controller
{
    public function index()
    {
        $trucks = Truck::all();
        return view('truck.index',compact('trucks'));
    }

    public function create()
    {
        $drivers = Category::where('category','Chauffeur')->first()->staffs;
        $assistants = Category::where('category','Assistant')->first()->staffs;

        return view('truck.create',compact('drivers', 'assistants'));
    }

    public function store(TruckRequest $request,Truck $truck,Product $product)
    {
        $request->request->add(['creator_id' => auth()->id()]);
        $truck->onCreate($request->all([
            "registered", "transporter", "creator_id", "assistant", "driver", "visit_technique","assurance"
        ]),$product);
        session()->flash('success', "Un nouveau Transport a bien été Ajouter");
        return redirect()->route('truck.show',compact('truck'));
    }

    public function show(Truck $truck)
    {
        $driver = null;
        $assistant = null;
        if($t = Driver::where([
            ['to', null],
            ['truck_id', $truck->id]
        ])->first()){
            $driver = $t->driver->full_name;
        }
        if($t = Assistant::where([
            ['to', null],
            ['truck_id', $truck->id]
        ])->first()){
            $assistant = $t->assistant->full_name;
        }
        // todo:: list of charges
        return view('truck.show',compact('truck','driver','assistant'));
    }

    public function edit(Truck $truck)
    {
        $drivers = Category::where('category','Chauffeur')->first()->staffs;
        $assistants = Category::where('category','Assistant')->first()->staffs;
        $current_assistant = Assistant::where([
            ['to', null],
            ['truck_id', $truck->id]
        ])->first();
        $current_driver = Driver::where([
            ['to', null],
            ['truck_id', $truck->id]
        ])->first();
        return view('truck.edit',compact('truck',"current_assistant","current_driver","drivers","assistants"));
    }

    public function update(TruckRequest $request, Truck $truck)
    {
        $truck->update($request->all(['registered', 'transporter', "assurance", "visit_technique"]));
        if($t = Driver::where([
            ['to', null],
            ['truck_id', $truck->id]
        ])->first()){
            $t->update(['to' => now()]);
        }
        Driver::create([
            'driver_id' =>  $request->driver,
            'from' => now(),
            'truck_id' =>  $truck->id
        ]);
        if($t = Assistant::where([
            ['to', null],
            ['truck_id', $truck->id]
        ])->first()){
            $t->update(['to' => now()]);
        }
        Assistant::create([
            'assistant_id' =>  $request->assistant,
            'from' => now(),
            'truck_id' =>  $truck->id
        ]);

        session()->flash('success', "Le Transport a bien été Mis à jour");
        return redirect()->route('truck.show',compact('truck'));
    }
}
