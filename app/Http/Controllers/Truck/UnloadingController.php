<?php

namespace App\Http\Controllers\Truck;

use App\Http\Controllers\Controller;
use App\Partner;
use App\Payment;
use App\Product;
use App\Stock;
use App\Truck;
use App\Unloading;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UnloadingController extends Controller
{

    public function index()
    {
        $unloadings = Unloading::all();
        return view('unloading.index',compact('unloadings'));
    }

    public function create()
    {
        $trucks = Truck::where('transporter', false)->get();
        $providers = Partner::where('provider', 1)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        $etrangers = Product::where('name', 'E Tranger')->get();
        $defecteusees = [];
        $consignees = [];
        foreach ($providers as $provider) {
            $consignees[$provider->id] = Product::where('name', Str::slug('consign ' . $provider->name, '_', 'fr_FR'))->get();
            $defecteusees[$provider->id] = Product::where('name', Str::slug('defecteuse ' . $provider->name, '_', 'fr_FR'))->get();
        }
        return view('unloading.create', compact("trucks", "providers", "gazes", "etrangers", "defecteusees", "consignees"));
    }

    public function store(Request $request)
    {
        // create unloading
        $unloading = Unloading::create([
            "truck_id" => $request->truck, "creator_id" => auth()->id(), "partner_id" => $request->provider
        ]);
        // create payment
        $this->addPayment($request,$unloading);
        // tmp
        $this->storeTmp($request, $unloading);
        session()->flash('success', "success");
        return redirect()->route('distributeur.links');
    }

    private function storeTmp($request, Unloading $unloading)
    {
        $provider = Partner::find($request->provider);

        foreach ($request->gaz as $key => $qt) {
            if (!is_null($qt) && $qt != 0) {
                $product_gaz = Product::find($key);
                $product_consign = Product::where([
                    ['bottle_size', $product_gaz->bottle_size],
                    ['name', Str::slug('consign_' . $provider->name, '_', 'fr_FR')]
                ])->first();
                $unloading->tmps()->create([
                    "qt" => $qt, "product_id" => $product_gaz->id
                ]);
                $unloading->tmps()->create([
                    "qt" => $qt, "product_id" => $product_consign->id
                ]);
                // unload from truck
                // load to store
                $truck_stock_gaz = Stock::where([
                    ['product_id', $product_gaz->id], ['truck_id', $request->truck]
                ])->first();
                $truck_stock_gaz->update([
                    "qt" => $truck_stock_gaz->qt - $qt
                ]);
                $truck_stock_consign = Stock::where([
                    ['product_id', $product_consign->id], ['truck_id', $request->truck]
                ])->first();
                $truck_stock_consign->update([
                    "qt" => $truck_stock_consign->qt - $qt
                ]);
                $store_stock_gaz = Stock::where([
                    ['product_id', $product_gaz->id], ['store_id', 1]
                ])->first();
                $store_stock_gaz->update([
                    "qt" => $store_stock_gaz->qt + $qt
                ]);
                $store_stock_consign = Stock::where([
                    ['product_id', $product_consign->id], ['store_id', 1]
                ])->first();
                $store_stock_consign->update([
                    "qt" => $store_stock_consign->qt + $qt
                ]);
            }
        }

        foreach ($request->defecteuse as $key => $qt) {
            $this->addStoreTmp($key, $qt, $unloading, $request->truck);
        }

        foreach ($request->etranger as $key => $qt) {
            $this->addStoreTmp($key, $qt, $unloading, $request->truck);
        }

        foreach ($request->consign as $key => $qt) {
            $this->addStoreTmp($key, $qt, $unloading, $request->truck);
        }

    }

    public function addPayment($request,Unloading $unloading)
    {
        $payment = [];
        $truck = Truck::find($request->truck);
        $cash = $truck->cash;
        $cheque = $truck->cheque;
        if ($request->cheque) {
            $cheque = Payment::create([
                "price" => $request->cheque, "nbr_operation" => $request->nbr_cheque, "mode_id" => 1
            ]);
            $payment[] = $cheque->id;
            $cheque = $cheque + $cheque->price;
        }
        if ($request->cash) {
            $cash = Payment::create([
                "price" => $request->cash, "mode_id" => 2
            ]);
            $payment[] = $cash->id;
            $cash = $cash + $cash->price;
        }
        if(isset($payment[0])){
            $unloading->payments()->attach($payment);
            $truck->update([
                "cash"      => $cash,
                "cheque"    => $cheque
            ]);
        }
    }

    private function addStoreTmp($key, $qt, $unloading, $truck_id)
    {
        if (!is_null($qt) && $qt != 0) {
            $product = Product::find($key);
            $unloading->tmps()->create([
                "qt" => $qt, "product_id" => $product->id
            ]);
            $truck_stock = Stock::where([
                ['product_id', $product->id], ['truck_id', $truck_id]
            ])->first();
            $store_stock = Stock::where([
                ['product_id', $product->id], ['store_id', 1]
            ])->first();
            $truck_stock->update([
                "qt" => $truck_stock->qt - $qt
            ]);
            $store_stock->update([
                "qt" => $store_stock->qt - $qt
            ]);
        }
    }

    public function edit(Unloading $unloading)
    {
        $trucks = Truck::where('transporter', false)->get();
        $providers = Partner::where('provider', 1)->get();
        $gazes = Product::where('name', 'Gaz')->get();
        foreach ($gazes as $gaze) {
            if($tmp = $unloading->tmps()->where('product_id',$gaze->id)->first()){
                $gaze->qt = $tmp->qt;
            }
        }

        $etrangers = Product::where('name', 'E Tranger')->get();
        foreach ($etrangers as $etranger) {
            if($tmp = $unloading->tmps()->where('product_id',$etranger->id)->first()){
                $etranger->qt = $tmp->qt;
            }
        }

        $consignees = [];
        $defecteusees = [];

        foreach ($providers as $provider) {
            $consignes = Product::where('name', Str::slug('consign ' . $provider->name, '_', 'fr_FR'))->get();
            $defecteuses = Product::where('name', Str::slug('defecteuse ' . $provider->name, '_', 'fr_FR'))->get();
            foreach ($consignes as $product) {
                if($tmp = $unloading->tmps()->where('product_id',$product->id)->first()){
                    $product->qt = $tmp->qt;
                }
            }
            $consignees[$provider->id] = $consignes;
            foreach ($defecteuses as $product) {
                if($tmp = $unloading->tmps()->where('product_id',$product->id)->first()){
                    $product->qt = $tmp->qt;
                }
            }
            $defecteusees[$provider->id] = $defecteuses;
        }

        return view('unloading.edit', compact("unloading", "trucks", "providers", "gazes", "etrangers", "defecteusees", "consignees"));
    }

    public function update(Request $request, Unloading $unloading)
    {
        // delete payments
        $this->subPayment($unloading);
        // create payment
        $this->addPayment($request,$unloading);
        // delete tmp
        $this->subStoreTmp($unloading);
        // update loading
        $unloading->update([
            "truck_id" => $request->truck,
            "partner_id" => $request->provider
        ]);
        // create tmp
        $this->storeTmp($request, $unloading);
        session()->flash('success', "update success");
        return redirect()->route('unloading.edit',compact('unloading'));
    }

    private function subStoreTmp(Unloading $unloading)
    {
        foreach ($unloading->tmps as $tmp) {
            $stock_truck = Stock::where([
                ['product_id', $tmp->product_id],
                ['truck_id', $unloading->truck_id]
            ])->first();
            $stock_store = Stock::where([
                ['product_id', $tmp->product_id],
                ['store_id', 1]
            ])->first();
            // sub from stock store
            $stock_store->update([
                'qt'    => $stock_store->qt - $tmp->qt
            ]);
            // add to stock truck
            $stock_truck->update([
                "qt"    => $stock_truck->qt + $tmp->qt
            ]);
            // delete tmps
            $tmp->delete();
        }
    }

    public function subPayment(Unloading $unloading)
    {
        if(isset($unloading->payments[0])){
            foreach ($unloading->payments as $payment) {
                $unloading->payments()->detach([$payment->id]);
                if($payment->mode_id == 1){
                    $unloading->truck->update([
                        'cheque'  => $unloading->truck->cheque  - $payment->price
                    ]);
                }
                if ($payment->mode_id == 2) {
                    $unloading->truck->update([
                        'cash' => $unloading->truck->cash - $payment->price
                    ]);
                }
                $payment->delete();
            }
        }
    }

}
