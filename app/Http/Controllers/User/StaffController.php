<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StaffStoreRequest;
use App\Http\Requests\User\StaffUpdateRequest;
use App\Staff;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StaffController extends Controller
{
    public function index()
    {
        $staffs = Staff::withTrashed()
            ->with(["owner","category"])
            ->orderBy('deleted_at',"asc")
            ->get();

        return view('user.index',compact('staffs'));
    }

    public function create()
    {
        $categories = Category::where("id", "!=", 1)->get();
        return view('user.create',compact('categories'));
    }

    public function store(StaffStoreRequest $request,Staff $staff)
    {
        $request->request->add([
            "creator_id" => auth()->id(),
            "category_id"   => $request->category
        ]);
        $staff = $staff->create($request->all([
            "last_name", "first_name", "mobile", "cin", "category_id", "creator_id"
        ]));

        if ((int) $request->category === 2 || (int) $request->category == 4) {
            $staff->owner()->create([
                "name"          => $request->name,
                "password"      => bcrypt($request->password),
                "remember_token"    => Str::random(10)
            ]);
        }

        session()->flash('success', "Une Nouvelle Personne a bien été ajouté");

        return redirect()->route('staff.index');

    }

    public function edit(Staff $staff)
    {
        return view('user.edit',compact('staff'));
    }

    public function update(StaffUpdateRequest $request, Staff $staff)
    {
        $staff->update($request->all(["last_name", "first_name", "mobile", "cin"]));
        session()->flash('success', 'Les informations de La personne a bien été modifier');
        return redirect()->route('staff.index');
    }

    public function destroy(Staff $staff)
    {
        if($staff->owner){
            $staff->owner->delete();
        }
        $staff->delete();
        session()->flash('success', "La Personne a bien été supprimé");
        return redirect()->route('staff.index');
    }

    public function restore(int $id)
    {
        $staff = Staff::withTrashed()->find($id);
        if($staff){
            $staff->restore();
            if($user = User::withTrashed()->where('staff_id',$id)->first()){
                $user->restore();
            }
            session()->flash('success', $staff->full_name . " a bien été débloquez");
            return redirect()->route('staff.index');
        }
        return abort(404);
    }
}
