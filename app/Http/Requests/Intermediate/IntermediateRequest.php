<?php

namespace App\Http\Requests\Intermediate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IntermediateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if (!$this->intermediate) {
            return [
                'name'  => "required|string|min:3|max:191|unique:intermediates,name"
            ];
        }
        return [
            'name'  => ["required","string","min:3","max:191",Rule::unique('intermediates')->ignore($this->intermediate->id)]
        ];
    }
}
