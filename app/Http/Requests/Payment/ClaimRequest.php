<?php

namespace App\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClaimRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "cheque"            => [Rule::requiredIf(function () {
                return (is_null($this->transfer));
            })],
            "transfer"          => [Rule::requiredIf(function () {
                return (is_null($this->cheque));
            })],
            "nbr_cheque"        => [Rule::requiredIf(function () {
                return (!is_null($this->cheque));
            })],
            "nbr_operation"     => [Rule::requiredIf(function () {
                return (!is_null($this->transfer));
            })],
            "bc.*"              => "nullable|exists:bcs,nbr"
        ];
    }

    public function attributes()
    {
        return ['bc.*' => "Bon de Commande"];
    }
}
