<?php

namespace App\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DebtRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "cheque"            => [Rule::requiredIf(function () {
                return (is_null($this->transfer));
            })],
            "transfer"          => [Rule::requiredIf(function () {
                return (is_null($this->cheque));
            })],
            "nbr_cheque"        => [Rule::requiredIf(function () {
                return (!is_null($this->cheque));
            })],
            "nbr_operation"     => [Rule::requiredIf(function () {
                return (!is_null($this->transfer));
            })],
            "inv.*"              => "nullable|exists:trades,nbr_inv"
        ];
    }

    public function attributes()
    {
        return ['inv.*' => "Numéro de Facture"];
    }
}
