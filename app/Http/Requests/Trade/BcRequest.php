<?php

namespace App\Http\Requests\Trade;

use Illuminate\Foundation\Http\FormRequest;

class BcRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "nbr" => "required|string",
            "provider" => "required|int|exists:partners,id",
            "transporter" => "required|int|exists:trucks,id",
            "nbr_cheque" => "nullable|string|required_with:cheque",
            "cheque" => "nullable|string|required_with:nbr_cheque",
            "nbr_operation" => "nullable|string|required_with:transfer",
            "transfer" => "nullable|string|required_with:nbr_operation",
            'intermediate' => "required|int|exists:intermediates,id"
        ];
    }

    public function attributes()
    {
        return [
            'nbr_cheque'    => "Nombre de chéque",
            'nbr_opération'    => "Nombre de d'opération",
        ];
    }
}
