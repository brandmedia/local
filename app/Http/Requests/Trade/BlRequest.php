<?php

namespace App\Http\Requests\Trade;

use Illuminate\Foundation\Http\FormRequest;

class BlRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "nbr" => "required|string",
            "provider" => "required|int|exists:partners,id",
            "transporter" => "required|int|exists:trucks,id",
            'intermediate' => "required|int|exists:intermediates,id"
        ];
    }
}
