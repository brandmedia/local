<?php

namespace App\Http\Requests\Truck;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChargeTruckRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'charge.*' => "required|int|exists:charges,id",
            'truck.*' => "required|string|exists:trucks,id",
            'price.*' => "required|int",
            'label.*' => "required|string|min:3|max:191",
            "cheque"            => [Rule::requiredIf(function () {
                return (is_null($this->transfer));
            })],
            "transfer"          => [Rule::requiredIf(function () {
                return (is_null($this->cheque));
            })],
            "nbr_cheque"        => [Rule::requiredIf(function () {
                return (!is_null($this->cheque));
            })],
            "nbr_operation"     => [Rule::requiredIf(function () {
                return (!is_null($this->transfer));
            })],
        ];
    }

    public function attributes()
    {
        return [
            'charge.*'      => "Charges",
            'truck.*'      => "Engin",
            'price.*'      => "Montant",
            'label.*'      => "Description"
        ];
    }
}
