<?php

namespace App\Http\Requests\User;

use App\Rules\MobileRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StaffStoreRequest
 * @property int category
 * @package App\Http\Requests\User
 */
class StaffStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'          => ['nullable', Rule::requiredIf(($this->category == 2 || $this->category == 4)), 'string', 'max:255', 'unique:users'],
            'password'      => ['nullable', Rule::requiredIf(($this->category == 2 || $this->category == 4)), 'string', 'min:8', 'confirmed'],
            'category'      => ['required', 'int', 'exists:categories,id'],
            'first_name'    => ['required', 'string', 'min:3', 'max:191'],
            'last_name'     => ['required', 'string', 'min:3', 'max:191'],
            'mobile'        => ['nullable', 'string', 'max:10', 'min:10', new MobileRules()],
            'cin'           => ['nullable', 'string', 'max:191', 'min:3'],
        ];
    }
}
