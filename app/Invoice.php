<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ["nbr", "from", "to"];
    protected $table = "invoices";

    public function trades()
    {
        return $this->belongsToMany(Trade::class, "invoice_trade");
    }

    public function payments()
    {
        return $this->belongsToMany(Payment::class, "invoice_payment");
    }

    public function invoice(Trade $trade)
    {
        $cycle = $this->cycle(now());
        $invoice = $this->where([
            ['from', $cycle['from']],
            ['to', $cycle['to']]
        ])->first();
        if ($invoice) {
            $this->add($trade, $invoice);
        }
        else {
            $invoice = $this->create([
                "nbr"       => $this->getIncrement(),
                "from"      => $cycle['from'],
                "to"        => $cycle["to"]
            ]);
            $this->add($trade,$invoice);
        }
    }

    private function cycle($date)
    {
        if (Carbon::parse($date)->format('d') < 15) {
            return [
                "from"  => Carbon::parse($date)->format('Y-m') . "-1",
                "to"    => Carbon::parse($date)->format('Y-m') . "-15"
            ];
        }
        return [
            ['from', Carbon::parse($date)->format('Y-m') . "-15"], ['to', Carbon::parse($date)->format('Y-m-t')]
        ];
    }

    private function add(Trade $trade, Invoice $invoice)
    {
        $invoice->trades()->attach($trade->id);
    }

    private function getIncrement()
    {
        if ($inc = $this->latest()->first()) {
            return (int) $inc->nbr + 1;
        }
        return 1;
    }
}
