<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loading extends Model
{
    protected $fillable = ["valid", "truck_id", "creator_id", "payment_id", "partner_id"];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function truck()
    {
        return $this->belongsTo(Truck::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function tmps()
    {
        return $this->hasMany(Tmp::class);
    }

}
