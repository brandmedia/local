<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ["qt", "ht", "tva", "ttc", "bc_id", "bl_id", "trade_id", "product_id"];

    public function bc()
    {
        return $this->belongsTo(Bc::class);
    }

    public function bl()
    {
        return $this->belongsTo(Bl::class);
    }

    public function trade()
    {
        return $this->belongsTo(Trade::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function onCreateBc(array $products, Trade $trade, Bc $bc)
    {
        foreach ($products as $product) {
            $product->orders()->create([
                "ht",
                "tva",
                "ttc",
                "bc_id"         => $bc->id,
                "trade_id"      => $trade->id,
            ]);
        }
    }
}
