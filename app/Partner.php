<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = [
        "provider", "account", "name", "speaker", "rc", "patent", "ice", "creator_id"
    ];

    public function incrementProvider()
    {
        $provider = $this->whereProvider(1)->latest()->first();
        if($provider){
            return $provider->account + 100;
        }
        return 900000;
    }

    public function incrementClient()
    {
        $client = $this->whereProvider(0)->latest()->first();
        if($client){
            return $client->account + 1;
        }
        return 100000;
    }

    public function getTurnoverAttribute()
    {
        if(isset($this->trades[0])) {
            return $this->trades()->sum('ttc');
        }
        return '0.00';
    }

    public function getSoldAttribute()
    {
        return 0 . " MAD";
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

    public function trades()
    {
        return $this->hasMany(Trade::class);
    }

    public function address()
    {
        return $this->hasOne(Address::class,'client_id');
    }

    public function loadings()
    {
        return $this->hasMany(Loading::class);
    }

    public function unloadings()
    {
        return $this->hasMany(Unloading::class,'creator_id');
    }

    public function claims()
    {
        return $this->hasMany(Claim::class,"provider_id");
    }

    public function onCreateProvider(array $data,Product $product)
    {
        $provider = $this->create([
            "provider"      => 1,
            "account"       => $this->incrementProvider(),
            "name"          => $data['name'],
            "speaker"       => $data['speaker'],
            "rc"            => $data['rc'],
            "patent"        => $data['patent'],
            "ice"           => $data['ice'],
            "creator_id"    => auth()->id()
        ]);

        $product->onCreate($provider);
    }

    public function onCreateClient(array $data)
    {
        $client = $this->create([
            "provider"      => 0,
            "account"       => $this->incrementClient(),
            "name"          => $data['name'],
            "speaker"       => $data['speaker'],
            "rc"            => $data['rc'],
            "patent"        => $data['patent'],
            "ice"           => $data['ice'],
            "creator_id"    => auth()->id()
        ]);
        // address
        $client->address()->create([
            'address'   => $data['address'],
            'city_id'   => $data['city']
        ]);
        // Discount
        $products = Product::where('name','Gaz')->get();
        foreach ($products as $product) {
            $product->discounts()->create([
                "price"         => 0,
                "partner_id"    => $client->id
            ]);
        }
    }
}
