<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ["price", "nbr_operation", "mode_id"];

    public function mode()
    {
        return $this->belongsTo(Mode::class);
    }

    public function chargeStores()
    {
        return $this->hasMany(ChargeStore::class);
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function truck()
    {
        return $this->belongsToMany(ChargeTruck::class, 'charge_truck_payment');
    }

    public function trades()
    {
        return $this->belongsToMany(Trade::class,'payment_trade');
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class,"invoice_payment");
    }

    public function loadings()
    {
        return $this->hasMany(Loading::class);
    }

    public function unloadings()
    {
        return $this->belongsToMany(Unloading::class,'payment_unloading');
    }

}
