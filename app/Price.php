<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['buy', 'sale', 'product_id', "creator_id"];

    public function getPriceRemplie(Product $consign, Stock $stock_consign)
    {
        $gaz = Product::where('bottle_size',$consign->bottle_size)->first();
        $gaz_ht = $this->getLastPriceBuy($gaz) * $stock_consign->qt;
        $gaz_tva = (($gaz_ht * $gaz->tva) / 100);
        $consign_ht = $this->getLastPriceBuy($consign) * $stock_consign->qt;
        $consign_tva = (($consign_ht * $consign->tva) / 100);
        return $gaz_ht + $gaz_tva + $consign_ht + $consign_tva;
    }

    private function getLastPriceBuy(Product $product)
    {
        if($buy = $product->prices()->latest('id')->first()) {
            return $buy->buy;
        }
        return 0;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

}
