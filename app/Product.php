<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $fillable = ["name", "bottle_size", "avg", "tva", "creator_id"];

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Create consignees and default prices For this Provider.
     * Create defectueuse and default prices For this Provider.
     * Add new Product in stock for Store and this Provider
     * Add Discount with all Clients
     * @param Partner $partner
     */
    public function onCreate(Partner $partner)
    {
        $sizes = ["3kg", "6kg", "12kg", "35kg"];

        foreach ($sizes as $size) {
            $this->createProduct($partner,'consign',$size);
            $this->createProduct($partner,'defecteuse',$size);
        }
    }

    private function createProduct(Partner $partner,string $name,string $size)
    {
        $product = $this->create([
            "name" => Str::slug($name . ' ' . $partner->name, '_', 'fr_FR'),
            "bottle_size" => $size, "avg" => 0,
            "tva" => 20,
            "creator_id" => $partner->creator_id
        ]);
        // Pour Le Fournisseur
        $product->stock()->create([
            'qt' => 0, 'partner_id' => $partner->id
        ]);
        // Pour Le Dépôt
        $product->stock()->create([
            'qt' => 0,
            'store_id' => 1
        ]);
        // pour truck
        $trucks = Truck::all();
        foreach ($trucks as $truck) {
            $product->stock()->create([
                'qt' => 0,
                'truck_id' => $truck->id
            ]);
        }
        $product->prices()->create(['creator_id' => auth()->id()]);
        $clients = Partner::where('provider', 0)->get();
        foreach ($clients as $client) {
            $product->discounts()->create([
                "price" => 0,
                "partner_id" => $client->id
            ]);
        }
    }

    public function onUpdateProvider(Partner $provider, string $name)
    {
        $products = $this->where('name', Str::slug('consign ' . $provider->name, '_', 'fr_FR'))->get();
        foreach ($products as $product) {
            $product->update([
                'name' => Str::slug('consign ' . $name, '_', 'fr_FR')
            ]);
        }
    }

    public function lastPrice()
    {
        if ($price = $this->prices()->latest('id')->first()) {
            return ['buy' => $price->buy, 'sale' => $price->sale];
        }
        return ['buy' => 0, 'sale' => 0];
    }

    public function getQtOrder($bc)
    {
        $value = $bc->orders()->where('product_id',$this->id)->first();
        return (!is_null($value)) ? $value->qt : null;
    }

    public function getDiscount(Partner $partner)
    {
        return $this->discounts()->where('partner_id',$partner->id)->first();
    }

}
