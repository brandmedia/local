<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MobileRules implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match("/^(0)[6-7]{1}[0-9]{8}$/", $value);
    }

    public function message()
    {
        return 'Ce numéro cellulaire n\'as pas une forme valide';
    }
}
