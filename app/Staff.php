<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;

    protected $fillable = ["last_name", "first_name", "mobile", "cin", "category_id", "creator_id"];

    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . strtoupper($this->last_name);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

    public function owner()
    {
        return $this->hasOne(User::class,'staff_id');
    }

    public function driver()
    {
        return $this->hasMany(Driver::class,'driver_id');
    }

    public function assistant()
    {
        return $this->hasMany(Assistant::class,'assistant_id');
    }

}
