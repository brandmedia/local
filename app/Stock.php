<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ["qt", "truck_id", "store_id", "partner_id", "product_id"];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function truck()
    {
        return $this->belongsTo(Truck::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function tmps()
    {
        return $this->hasMany(Tmp::class);
    }

}
