<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tmp extends Model
{
    protected $fillable = ["qt", "loading_id", "unloading_id", "product_id"];

    public function loading()
    {
        return $this->belongsTo(Loading::class);
    }

    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }
}
