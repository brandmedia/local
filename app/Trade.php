<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    protected $fillable = [
        "trade", "slug_inv", "nbr_inv", "ht", "tva", "ttc",
        "partner_id", "creator_id", "intermediate_id", "truck_id"
    ];


    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

    public function intermediate()
    {
        return $this->belongsTo(Intermediate::class);
    }

    public function truck()
    {
        return $this->belongsTo(Truck::class);
    }

    public function payments()
    {
        return $this->belongsToMany(Payment::class,'payment_trade');
    }

    public function bc()
    {
        return $this->hasOne(Bc::class);
    }

    public function bl()
    {
        return $this->hasOne(Bl::class);
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class,"invoice_trade");
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function chargeTruck()
    {
        return $this->hasOne(ChargeTruck::class);
    }

    public function claimDetails()
    {
        return $this->hasMany(ClaimDetail::class);
    }

    public function getSaleIncrements()
    {
        $last = $this->latest('id')->first();
        if(!$last){
            $year = Carbon::now()->format('y');
            $month = Carbon::now()->format('m');
            $nbr = 1;
        }
        else{
            return explode('-',$last->nbr_inv)[3] + 1;
        }
        return $month . '-' . $year . '-' . auth()->id() . '-' . $nbr;
    }
}
