<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    protected $fillable = [
        "registered", "transporter", "lat", "lang", "cash", "cheque", "assurance", "visit_technique" ,"creator_id"
    ];

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }

    public function charges()
    {
        return $this->hasMany(ChargeTruck::class,'truck_id');
    }

    public function trades()
    {
        return $this->hasMany(Trade::class);
    }

    public function driver()
    {
        return $this->hasMany(Driver::class);
    }

    public function assistant()
    {
        return $this->hasMany(Assistant::class);
    }

    public function loadings()
    {
        return $this->hasMany(Loading::class);
    }

    public function unloadings()
    {
        return $this->hasMany(Unloading::class,'creator_id');
    }

    public function onCreate(array $data,Product $product)
    {
        $truck = $this->create([
            "registered"        => $data['registered'],
            "transporter"       => $data['transporter'],
            "creator_id"        => $data['creator_id']
        ]);
        $products = $product->get();
        foreach ($products as $p) {
            $truck->stock()->create([
                "qt"            => 0,
                "product_id"    => $p->id
            ]);
        }
        // detach driver and assistant if isset
        if($driver = Driver::where([
            ['driver_id',$data['driver']],
            ['to', null]
        ])->first()){
            $driver->update(['to' => now()]);
        }
        if($assistant = Assistant::where([
            ['assistant_id',$data['assistant']],
            ['to', null]
        ])->first()){
            $assistant->update(['to' => now()]);
        }
        // attach with driver and assistant
        Driver::create([
            "driver_id"         => $data['driver'],
            "truck_id"          => $truck->id,
            "from"              => now()
        ]);
        Assistant::create([
            "assistant_id"      => $data['assistant'],
            "truck_id"          => $truck->id,
            "from"              => now()
        ]);
    }
}
