<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unloading extends Model
{
    protected $fillable = ["truck_id", "creator_id", "partner_id"];

    public function getNbrChequeAttribute()
    {
        if($payment = $this->payments()->where('mode_id',1)->first()){
            return $payment->nbr_operation;
        }
        return null;
    }
    public function getChequeAttribute()
    {
        if($payment = $this->payments()->where('mode_id',1)->first()){
            return $payment->price;
        }
        return null;
    }
    public function getCashAttribute()
    {
        if($payment = $this->payments()->where('mode_id',2)->first()){
            return $payment->price;
        }
        return null;
    }
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function truck()
    {
        return $this->belongsTo(Truck::class);
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }
    // payment many to many => get with sum
    public function payments()
    {
        return $this->belongsToMany(Payment::class,"payment_unloading");
    }

    public function tmps()
    {
        return $this->hasMany(Tmp::class);
    }

}
