<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'password', "staff_id"
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = [
        'created_at', "updated_at"
    ];

    public function getIsAdminAttribute()
    {
        return $this->staff->category->category === "Gérant";
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class,'staff_id');
    }

    /**
     * Les Produits que j'ai ajouté
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdProducts()
    {
        return $this->hasMany(Product::class,'creator_id');
    }

    /**
     * Les Prix que j'ai ajouté
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productPrices()
    {
        return $this->hasMany(Price::class,'creator_id');
    }

    /**
     * La liste de tous les Camions Transporteurs et distributeurs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trucks()
    {
        return $this->hasMany(Truck::class,'creator_id');
    }

    public function partners()
    {
        return $this->hasMany(Partner::class,'creator_id');
    }

    public function staffs()
    {
        return $this->hasMany(Staff::class,'creator_id');
    }

    public function chargeTrucks()
    {
        return $this->hasMany(ChargeTruck::class);
    }

    public function chargeStores()
    {
        return $this->hasMany(ChargeStore::class,'creator_id');
    }

    public function trades()
    {
        return $this->hasMany(Trade::class,'creator_id');
    }

    public function loadings()
    {
        return $this->hasMany(Loading::class,'creator_id');
    }

    public function unloadings()
    {
        return $this->hasMany(Unloading::class,'creator_id');
    }


}
