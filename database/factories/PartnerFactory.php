<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Partner;
use Faker\Generator as Faker;

$factory->define(Partner::class, function (Faker $faker) {
    return [
        'provider'  => rand(0,3),
        'name'      => $faker->company,
        'speaker'   => $faker->name,
        'rc'        => $faker->numberBetween(),
        'patent'    => $faker->numberBetween(),
        'ice'       => $faker->numberBetween(),
        'creator_id' => 1
    ];
});
