<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Staff;
use Faker\Generator as Faker;

$factory->define(Staff::class, function (Faker $faker) {
    return [
        [
            "last_name" => (string) $faker->lastName,
            "first_name" =>  (string) $faker->firstName,
            "mobile" =>  (string) $faker->phoneNumber,
            "cin" =>  (string) "bh456789",
            "creator_id" => 1
        ],
    ];
});
