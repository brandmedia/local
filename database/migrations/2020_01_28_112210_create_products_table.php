<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedDecimal('avg')
                ->nullable()
                ->comment('le prix moyen de vente pour autre produits');
            $table->unsignedInteger('tva')
                ->comment('le pourcentage de la TVA');
            $table->string('bottle_size')->nullable();
            $table->unsignedBigInteger('creator_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
