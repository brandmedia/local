<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrucksTable extends Migration
{
    public function up()
    {
        Schema::create('trucks', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('registered')
                ->comment("Numéro d'imatriculation");
            $table->boolean('transporter')
                ->default(0)
                ->comment("0 => Distributor, 1 => Transporter");
            $table->string('lat')->nullable();
            $table->string('lang')->nullable();
            $table->decimal('cash')->default(0);
            $table->decimal('cheque')->default(0);
            $table->date("assurance")->nullable();
            $table->date("visit_technique")->nullable();
            $table->unsignedBigInteger('creator_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('trucks');
    }
}
