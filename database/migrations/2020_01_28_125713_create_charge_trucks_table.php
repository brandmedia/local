<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChargeTrucksTable extends Migration
{
    public function up()
    {
        Schema::create('charge_trucks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('truck_id');
            $table->unsignedBigInteger('charge_id');
            $table->unsignedBigInteger('trade_id');
            $table->string('label')->nullable();
            $table->boolean('creator_id')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('charge_trucks');
        Schema::dropIfExists('charge_truck_payment');
    }
}
