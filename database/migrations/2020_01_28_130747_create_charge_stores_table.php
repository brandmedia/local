<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChargeStoresTable extends Migration
{
    public function up()
    {
        Schema::create('charge_stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('creator_id');
            $table->timestamps();
        });

        Schema::create('charge_store_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payment_id');
            $table->unsignedBigInteger('charge_store_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('charge_stores');
        Schema::dropIfExists('charge_store_payment');
    }
}
