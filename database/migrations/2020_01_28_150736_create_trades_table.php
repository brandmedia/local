<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradesTable extends Migration
{
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('trade')
                        ->comment("
                        0 => buy bottle, 
                        1 => sale bottle, 
                        2 => bc, 
                        3 => bl, 
                        4 => charge truck, 
                        ");

            $table->string('slug_inv')->nullable();
            $table->string('nbr_inv')->nullable();
            $table->decimal('ht')->default(0);
            $table->decimal('tva')->default(0);
            $table->decimal('ttc')->default(0);

            $table->unsignedBigInteger('partner_id');
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('intermediate_id')->nullable();
            $table->unsignedBigInteger('truck_id')->nullable();
            $table->timestamps();
        });

        Schema::create('payment_trade', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trade_id');
            $table->unsignedBigInteger('payment_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('trades');
        Schema::dropIfExists('payment_trade');
    }
}
