<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBcsTable extends Migration
{
    public function up()
    {
        Schema::create('bcs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nbr');
            $table->unsignedDecimal('tva_gaz')->default(0);
            $table->unsignedDecimal('tva_consign')->default(0);
            $table->unsignedBigInteger('trade_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bcs');
    }
}
