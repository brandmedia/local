<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntermediatesTable extends Migration
{
    public function up()
    {
        Schema::create('intermediates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('consign')->comment("he buy consign or not");
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intermediates');
    }
}
