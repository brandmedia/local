<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('qt');
            $table->unsignedDecimal('ht');
            $table->unsignedDecimal('tva');
            $table->unsignedDecimal('ttc');
            $table->unsignedBigInteger('bc_id')->nullable();
            $table->unsignedBigInteger('bl_id')->nullable();
            $table->unsignedBigInteger('trade_id');
            $table->unsignedBigInteger('product_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
