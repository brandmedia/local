<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadingsTable extends Migration
{
    public function up()
    {
        Schema::create('loadings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('valid')->default(0);
            $table->unsignedBigInteger('nbr');
            $table->unsignedBigInteger('truck_id');
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('partner_id');
            $table->unsignedBigInteger('payment_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('loadings');
        Schema::dropIfExists('loading_payment');
    }
}
