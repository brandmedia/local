<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpsTable extends Migration
{
    public function up()
    {
        Schema::create('tmps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('qt');
            $table->unsignedBigInteger('loading_id')->nullable();
            $table->unsignedBigInteger('unloading_id')->nullable();
            $table->unsignedBigInteger('product_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tmps');
    }
}
