<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnloadingsTable extends Migration
{
    public function up()
    {
        Schema::create('unloadings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('truck_id');
            $table->unsignedBigInteger('nbr');
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('partner_id');
            $table->timestamps();
        });
        Schema::create('payment_unloading', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('payment_id');
            $table->unsignedBigInteger('unloading_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('unloadings');
        Schema::dropIfExists('payment_unloading');
    }
}
