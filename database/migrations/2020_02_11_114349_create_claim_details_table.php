<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('claim_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trade_id');
            $table->unsignedBigInteger('claim_id');
            $table->unsignedBigInteger("bc_nbr");
            $table->unsignedDecimal('term');
            $table->string('inv');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('claim_details');
    }
}
