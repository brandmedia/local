<?php

use Illuminate\Database\Seeder;

class BcsTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            "nbr" => "123456",
            "provider" => 3,
            "transporter" => 1,
            "nbr_cheque" => "123",
            "cheque" => "100",
            "nbr_operation" => "654",
            "transfer" => "100",
            "a_term"    => "196",
            'intermediate' => 1,
            "consign"   => [],
            "gaz"       => [
                1 => 12
            ]
        ];
        $bc = new \App\Bc();
        $bc->onCreate($data);
    }
}
