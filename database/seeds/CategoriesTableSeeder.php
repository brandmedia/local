<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $categories = ["Gérant", "Chauffeur", "Assistant", "Dépôt"];

        foreach ($categories as $category) {
            Category::create(['category'   => $category]);
        }

    }
}
