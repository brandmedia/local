<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        // seed needed data for installation App
        $this->call(InstallerAppSeeder::class);
        // seed needed data for dev
        $this->call([
            TrucksTableSeeder::class,
            PricesTableSeeder::class,
            IntermediatesTableSeeder::class,
            StockTableSeeder::class,
            BcsTableSeeder::class
        ]);
    }
}
