<?php

use App\User;
use Illuminate\Database\Seeder;

class InstallerAppSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            CitiesTableSeeder::class,
            CategoriesTableSeeder::class,
            ChargesTableSeeder::class,
            ModesTableSeeder::class,
            ProductsTableSeeder::class,
        ]);
        factory(User::class,1)->create([
                'name'      => "Admin",
                'staff_id' => 1
            ]);
        auth()->setUser(User::find(1));
        auth()->user()->staff()->create([
            "last_name" => (string) "Admin",
            "first_name" =>  (string) "ADMIN",
            "mobile" =>  (string) "0600000000",
            "cin" =>  (string) "bh446789",
            'category_id'   => 1,
            "creator_id" => 1
        ]);

        $this->call(PartnersTableSeeder::class);

    }
}
