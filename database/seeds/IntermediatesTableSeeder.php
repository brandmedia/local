<?php

use App\Intermediate;
use Illuminate\Database\Seeder;

class IntermediatesTableSeeder extends Seeder
{
    public function run()
    {
        $intermediates = [
            ["name" => "ziz", "consign" => false],
            ["name" => "SALAM Gaz", "consign" => true]
        ];
        foreach ($intermediates as $intermediate) {
            Intermediate::create([
                'name'  => $intermediate['name'],
                'consign'  => $intermediate['consign'],
            ]);
        }
    }
}
