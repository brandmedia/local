<?php

use Illuminate\Database\Seeder;

class ModesTableSeeder extends Seeder
{
    public function run()
    {
        $modes = ["cheque", "cash", "transfer", "term"];
        foreach ($modes as $mode) {
            \App\Mode::create([
                'mode'  => $mode
            ]);
        }
    }
}
