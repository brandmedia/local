<?php

use App\Partner;
use App\Product;
use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{
    public function run()
    {
        Partner::create([
            "provider"      => 3,
            "account"       => 400000,
            "name"          => "Particulier",
            "speaker"       => "Particulier",
            "rc"            => 0,
            "patent"        => 0,
            "ice"           => 0,
            "creator_id"    => 1
        ]);
        Partner::create([
            "provider"      => 2,
            "account"       => 500000,
            "name"          => "Fournisseur Autre",
            "speaker"       => "Fournisseur Autre",
            "rc"            => 0,
            "patent"        => 0,
            "ice"           => 0,
            "creator_id"    => 1
        ]);
        // add partner Client and Provider
        $this->createProvider(new Partner(),new Product());
        $this->createClient(new Partner());

    }

    private function createProvider(Partner $provider, Product $product)
    {
        $data = [
            "name"          => "ziz",
            "speaker"       => "Gérant",
            "rc"            => "213456",
            "patent"        =>"5646213",
            "ice"           => "654132",
        ];

        $provider->onCreateProvider($data,$product);
        $data = [
            "name"          => "Aqua",
            "speaker"       => "Gérant",
            "rc"            => "213456",
            "patent"        =>"5646213",
            "ice"           => "654132",
        ];

        $provider->onCreateProvider($data,$product);
    }

    private function createClient(Partner $client)
    {
        $data = [
            "name"          => "Particulier",
            "speaker"       => "Gérant",
            "rc"            => "213456",
            "patent"        =>"5646213",
            "ice"           => "654132",
            "address"       => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "city"          => 1,
        ];

        $client->onCreateClient($data);
    }
}
