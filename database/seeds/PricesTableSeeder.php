<?php

use App\Price;
use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    public function run()
    {
        $prices = Price::where('id', '>', 12)->get();
        foreach ($prices as $price) {
            $price->update([
                'buy' => 80, "sale" => 100
            ]);
        }
    }
}
