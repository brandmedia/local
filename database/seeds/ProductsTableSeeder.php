<?php

use App\Product;
use App\Store;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $this->create();
    }

    private function create()
    {
        $store = Store::create(["name" => 'Dépot']);
        $names = ["3kg", "6kg", "12kg", "35kg"];
        $categories = ["Gaz", 'E Tranger'];
        $products = [];
        $bottles = [];
        foreach ($names as $key => $name) {
            foreach ($categories as $category) {
                $products[] = $category;
                $bottles[] = $name;
            }
        }
        foreach ($products as $key => $product) {
            if($product == 'Gaz'){
                $p = $this->product([
                    'name' => $product, 'avg' => 0, 'tva' => 10, 'bottle_size' => $bottles[$key], 'creator_id' => 1
                ]);
                $this->stock($p,['qt' => 0, 'store_id' => 1]);
                $this->prices($p,['buy' => 30, 'sale' => 40, "creator_id" => 1]);
            }
            else{
                $p = $this->product([
                    'name' => $product, 'avg' => 0, 'tva' => 20, 'bottle_size' => $bottles[$key], 'creator_id' => 1
                ]);
                $this->stock($p,['qt' => 0, 'store_id' => 1]);
                $this->prices($p,['buy' => 80, 'sale' => 100, "creator_id" => 1]);
            }

        }
    }

    private function product(array $data)
    {
        return Product::create($data);
    }

    private function prices(Product $product,array $data)
    {
        $price = $product->prices()->create($data);
    }

    private function stock(Product $product,array $data)
    {
        return $product->stock()->create([
            'qt'        => $data['qt'],
            'store_id' => $data['store_id']
        ]);
    }

}
