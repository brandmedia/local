<?php

use Illuminate\Database\Seeder;

class StockTableSeeder extends Seeder
{
    public function run()
    {
        $stocks = \App\Stock::all();
        foreach ($stocks as $stock) {
            $stock->update([
                'qt'    => 100
            ]);
        }
    }
}
