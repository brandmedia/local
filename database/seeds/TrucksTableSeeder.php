<?php

use App\Product;
use App\Truck;
use Illuminate\Database\Seeder;

class TrucksTableSeeder extends Seeder
{

    public function run()
    {
        $trucks = [
            [
                'registered' => "1234564879",
                "transporter" => 1,
                'creator_id' => 1,
                "driver" => 2,
                "assistant" => 3
            ],
            [
                'registered' => "5646791568",
                "transporter" => 0,
                'creator_id' => 1,
                "driver" => 2,
                "assistant" => 3
            ],
        ];
        $product = new Product();
        $truck = new Truck();
        foreach ($trucks as $t) {
            $truck->onCreate($t,$product);
        }
    }
}
