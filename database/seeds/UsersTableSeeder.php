<?php

use App\Category;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        foreach (Category::all() as $category) {
            factory(\App\User::class,1)->create([
                'name'      => $category->category,
                'category_id'   => $category->id,
                'staff_id' => 1
            ]);
        }
    }
}
