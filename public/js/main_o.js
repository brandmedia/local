$(document).ready(function() {

    $('input').on("keypress", function(e) {
        // update submit input to Tab
        if (e.keyCode === 13) {
            var inputs = $(this).parents("table").eq(0).find(":input");
            var indexOfInput= inputs.index(this);

            if (indexOfInput < inputs.length - 1) {
                inputs[indexOfInput + 1].focus(); 
            }
        }
    });

    // content loading
    setTimeout(function(){
        $('#contentloading').hide();
    },10);

    // sidebar animate
    $('#to_small_sidebar').on('click',function(){
        var current_with = $('.adm .admin-header .left-part').css('width');
        if(current_with == '260px'){
            localStorage.setItem(to_small_sidebar, 'small');
            toSmallNav();
            
        }else{
            localStorage.setItem(to_small_sidebar, 'half');
            toHalfNav();
        }
    });


    

    
      

});



/*  ================== functions =================  */
function toSmallNav(){
    $('.adm .admin-header .left-part, .adm .main-sidebar').animate({width:'105px'});
    $('.adm .admin-header .right-part , .adm .content_section').animate({marginLeft: '105px'});
    $('.adm .admin-header .left-part .logo-lg').html('Cpanel');
    $('.adm .main-sidebar .user_panel').css('padding','20px 0');
    $('.adm .main-sidebar .user_panel > div > div  ').attr('class', '').addClass('col-md-12');
    $('.adm .main-sidebar .user_panel > div > div > div ').css('text-align','center');
    $('.adm .main-sidebar .sidebar-menu > li > a span').hide();
}

function toHalfNav(){
    $('.adm .admin-header .left-part, .adm .main-sidebar').animate({width:'260px'});
    $('.adm .admin-header .right-part , .adm .content_section').animate({marginLeft: '260px'});
    $('.adm .admin-header .left-part .logo-lg').html('Gaz Control Panel ');
    $('.adm .main-sidebar .user_panel').css('padding','20px 30px');
    $('.adm .main-sidebar .user_panel > div > div  ').attr('class', '').addClass('col-sm-12');
    $('.adm .main-sidebar .user_panel > div > div > div ').css('text-align','left');
    $('.adm .main-sidebar .sidebar-menu > li > a span').show();

}
