@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('charge.store') }}" method="POST">
            @csrf
            <table class="table table-bordered text-center table-dynamic-rows table-date-filter">
                <thead>
                <tr>
                    <th colspan="4">Saisie Charge :</th>
                </tr>

                </thead>
                <tbody style="background: #7cb3b9;">
                @if(old('truck'))
                    @foreach(old('truck') as $key => $truck)
                        <tr>
                            <td>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="truck[{{$key}}]" title="truck" id="truck" class="form-control" required>
                                            @foreach($trucks as $truck)
                                                <option value="{{ $truck->id }}"
                                                        {{ (old('truck')[$key] === $truck->id) ? 'selected' : '' }}>
                                                    {{ $truck->registered }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('truck.' . $key))
                                            <span class="text-danger">{{ $errors->first('truck.' . $key) }}</span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="charge[{{ $key }}]" title="charge" id="charge" class="form-control">
                                            @foreach($charges as $charge)
                                                <option value="{{ $charge->id }}"
                                                        {{ (old('charge.' . $key) === $charge->id) ? 'selected' : '' }}>
                                                    {{ $charge->charge }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('charge.' . $key))
                                            <br><span class="text-danger">{{ $errors->first('charge.' . $key) }}</span>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                <input type="text" name="label[{{ $key }}]" value="{{ old('label.' . $key) }}" class="btn-spanen col-md-12" placeholder="label" required>
                                @if($errors->has('label.' . $key))
                                    <br><span class="text-danger">{{ $errors->first('label.' . $key) }}</span>
                                @endif
                            </td>
                            <td>
                                <input type="text" name="price[{{$key}}]" value="{{ old('price.' . $key) }}" class="btn-spanen" placeholder="montant" required>
                                @if($errors->has('price.' . $key))
                                    <br><span class="text-danger">{{ $errors->first('price.' . $key) }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select name="truck[]" title="truck" id="truck" class="form-control" required>
                                        @foreach($trucks as $truck)
                                            <option value="{{ $truck->id }}"
                                                    {{ (old('truck') === $truck->id) ? 'selected' : '' }}>
                                                {{ $truck->registered }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('truck'))
                                        <span class="text-danger">{{ $errors->first('truck') }}</span>
                                    @endif
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select name="charge[]" title="charge" id="charge" class="form-control">
                                        @foreach($charges as $charge)
                                            <option value="{{ $charge->id }}"
                                                    {{ (old('charge') === $charge->id) ? 'selected' : '' }}>
                                                {{ $charge->charge }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('charge'))
                                        <span class="text-danger">{{ $errors->first('charge') }}</span>
                                    @endif
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="text" name="label[]" class="btn-spanen col-md-12" placeholder="label" required>
                        </td>
                        <td>
                            <input type="text" name="price[]" class="btn-spanen" placeholder="montant" required>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>

            <br>
            <div class="text-left">
                <div class="col-md-6 text-left">
                    <div class="text-left">
                        <h5 class="mode_paiement_title">Mode de Paiement </h5>
                        <table>
                            <tr>
                                <td><h5>Chéque </h5></td>
                                <td><input type="text" name="cheque" value="{{ old('cheque') }}"
                                           placeholder="montant" class="btn-spanen">
                                    <input type="text" name="nbr_cheque" value="{{ old('nbr_cheque') }}"
                                           placeholder="numéro de chéque"
                                           class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @if($errors->has('nbr_cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('nbr_cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td><h5>Virement </h5></td>
                                <td>
                                    <input type="text" name="transfer" value="{{ old('transfer') }}"
                                           placeholder="virement" class="btn-spanen">
                                    <input type="text" name="nbr_operation" value="{{ old('nbr_operation') }}"
                                           placeholder="virement"
                                           class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('transfer'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('transfer') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @if($errors->has('nbr_operation'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('nbr_operation') }}</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td><h5>A terme </h5></td>
                                <td><input type="text" name="a_term" value="{{ old('a_term') }}"
                                           placeholder="Montant" class="btn-spanen"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                @if($errors->has('payment'))
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="text-danger">{{ $errors->first('payment') }}</span>
                        </div>
                    </div>
                @endif
            </div>
            <button class="btn-imprimer"><i class="fas fa-file-download"></i> Validé</button>
        </form>
    </div>
@endsection

