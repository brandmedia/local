@extends('layouts.app')

@section('content')
    <div class="container">
        <div style="max-width:600px;text-align:left">
            <table id="example" class="display dataTables_wrapper" style="width:100%;">
                <thead>
                <tr>
                    <th class="text-left">Compte</th>
                    <th class="text-left">Client</th>
                    <th class="text-left">Raison Sociale</th>
                    <th class="text-left">Ville</th>
                    <th>Chiffre d'affaire</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($clients[0]))
                    @foreach($clients as $client)
                        <tr>
                            <td class="text-left">{{ $client->account }}</td>
                            <td class="text-left">{{ $client->name }}</td>
                            <td class="text-left">{{ $client->speaker }}</td>
                            <td class="text-left">{{ $client->address->city->city }}</td>
                            <td>{{ $client->turnover }} MAD</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4" class="text-center">Pas de Client pour Le moment Veuillez Ajouté un nouveau</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
