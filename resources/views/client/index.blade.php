@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-5">
            @if(auth()->user()->is_admin)
                <div class="col-md-6">
                    <div class="float-right">
                        <a href="{{ route('client.create') }}"
                           class="btn btn-success text-right"
                        >Ajouter Un nouveau Client</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="float-right">
                        <a href="{{ route('client.all') }}"
                           class="btn btn-secondary text-right"
                        ><i class="fa fa-print"></i> Imprimé Tous Les Clients</a>
                    </div>
                </div>
            @endif
        </div>
        @isset($clients[0])
            <div class="row mb-5">
                <div class="col-md-6">
                    <form action="{{ route('client.search.name') }}" method="POST">
                        @csrf
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="name">Rechercher par Raison Sociale :</label>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="search" name="name" id="name"
                                               value="{{ (isset($name)) ? $name : '' }}"
                                               placeholder="Rechercher par Raison Social:" class="form-control">
                                    </div>
                                    <div class="col-md-3">
                                        <span class="btn btn-primary"><i class="fa fa-search"></i></span>
                                    </div>
                                </div>
                                @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form action="{{ route('client.search.city') }}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="city">Rechercher par ville :</label>
                                <div class="row">
                                    <div class="col-xs-10 mr-0">
                                        <select name="city" id="city" class="form-control" required>
                                            <option disabled selected>Rechercher par ville :</option>
                                            @foreach($cities as $city)
                                                <option value="{{ $city->id }}"
                                                        @if(isset($citie) && $citie == $city->id)
                                                        selected
                                                        @endif
                                                >{{ $city->city }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('city'))
                                            <span class="text-danger">{{ $errors->first('city') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-2 ml-0">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        @endisset
        <div style="max-width:600px;text-align:left">
            <table id="example" class="display dataTables_wrapper" style="width:100%;">
                <thead>
                <tr>
                    <th class="text-left">Compte</th>
                    <th class="text-left">Client</th>
                    <th class="text-left">Gérant</th>
                    <th>Chiffre d'affaire</th>
                    <th colspan="2" class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($clients[0]))
                    @foreach($clients as $client)
                        <tr>
                            <td class="text-left">{{ $client->account }}</td>
                            <td class="text-left">{{ $client->name }}</td>
                            <td class="text-left">{{ $client->speaker }}</td>
                            <td>{{ $client->turnover }} MAD</td>
                            <td>
                                <a href="{{ route('client.show',compact('client')) }}"
                                   class="btn btn-success btn-sm"> <i class="fas fa-eye"></i> Compte Client</a>
                            </td>
                            <td>
                                <a href="{{ route('discount.create',compact('client')) }}"
                                   class="btn btn-success btn-sm"> Remise</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4" class="text-center">Pas de Client pour Le moment Veuillez Ajouté un nouveau</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        @if(request()->is(route('client.search.city')))
            {{ $clients->links() }}
        @endif
    </div>
@endsection
