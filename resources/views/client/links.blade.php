@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page_links">
            <div class="row">
                <div class="col-md-6">
                    <a href="{{ route('client.index') }}" ><i class="fas fa-clipboard-list"></i> Liste des Clients </a>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('client.create') }}" > <i class="fas fa-user-plus"></i> Ajouter un Client </a>
                </div>
                <div class="col-md-6">
                    <a href="#" > <i class="fas fa-user-plus"></i> Liste des Client Débiteur </a>
                </div>
            </div>
        </div>
    </div>
@endsection
