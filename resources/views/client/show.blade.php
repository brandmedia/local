@extends('layouts.app')

@section('content')
    <div class="container page-cleint-historique">
        <div class="row">
            <div class="col-md-4">
                <table>
                    <tr class="mb-4">
                        <td style="padding: 10px 0;">Compte Client</td>
                        <td><span class="span_designed"><b>{{ $client->account }}</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Raison Sociale</td>
                        <td><span class="span_designed"><b>{{ $client->name }}</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Gérant</td>
                        <td><span class="span_designed"><b>{{ $client->speaker }}</b></span></td>
                    </tr>
                    <tr>
                        <td colspan="2"><br><br><br></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Chiffre d'affaire</td>
                        <td><span class="span_designed"><b>{{ $client->turnover }} MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Bénéfice</td>
                        <td><span class="span_designed"><b>?? MAD</b></span></td>
                    </tr>
                </table>

            </div>
            <div class="col-md-4">
                <table class="table table-no-border table-date-filter">
                    <tr>
                        <td>De</td>
                        <td><input type="date"></td>
                    </tr>
                    <tr>
                        <td>A</td>
                        <td>
                            <input type="date">
                            <button class="btn-search-filterr" type="button"><i class="fas fa-search"></i></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <div class="float-right">
                    <table class="table table-bordered" style="max-width: 150px;">
                        <thead>
                        <tr>
                            <th>QT</th>
                            <th>Gaz</th>
                            <th>Consign</th>
                            <th>Défectueuse</th>
                            <th>Etranger</th>
                        </tr>
                        </thead>
                        <tr>
                            <td><b>3KG</b></td>
                            <td><span class="span_designed">1450</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                        </tr>
                        <tr>
                            <td><b>6KG</b></td>
                            <td><span class="span_designed">2250</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                        </tr>
                        <tr>
                            <td><b>12KG</b></td>
                            <td><span class="span_designed">165</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                        </tr>
                        <tr>
                            <td><b>35KG</b></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">0</span></td>
                            <td><span class="span_designed">225</span></td>
                            <td><span class="span_designed">0</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="btn btn-lg btn-solde">Solde <span>{!! $client->sold !!}</span></div>
        <br>
        <table id="example" class="display dataTables_wrapper" style="width:100%">
            <thead>
            <tr>
                <th>Date</th>
                <th>Fournisseur</th>
                <th>Libellé</th>
                <th>Détails</th>
                <th>QN Entre</th>
                <th>QN Sortie</th>
                <th>Débit</th>
                <th>Crédit</th>
            </tr>
            </thead>
            <tbody>
            @isset($client->trades[0])
                @foreach($client->trades as $trade)
                    @if($trade->bc)
                        @foreach($trade->bc->orders as $order)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($trade->created_at)->format('d / m / y') }}</td>
                                <td>{{ $trade->bc->intermediate->name }}</td>
                                <td>BC {{ $order->product->name }} n° {{ $trade->bc->nbr }}</td>
                                <td>{{ $order->product->bottle_size }}</td>
                                <td>{{ ($order->product->name == 'consigne') ? '' : $order->qt }}</td>
                                <td></td>
                                <td>{{ $order->ht }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                    @if($trade->bl)
                        @foreach($trade->bl->orders as $order)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($trade->created_at)->format('d / m / y') }}</td>
                                <td>{{ $trade->bl->intermediate->name }}</td>
                                <td>BL {{ $order->product->name }} n° {{ $trade->bl->nbr }}</td>
                                <td>{{ $order->product->bottle_size }}</td>
                                <td></td>
                                <td>{{ $order->qt }}</td>
                                <td></td>
                                <td>{{ $order->ht }}</td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="8">Pas de transaction Pour le moment</td>
                    </tr>
                    @endisset
            </tbody>
        </table>
    </div>
@endsection

