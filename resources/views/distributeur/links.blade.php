@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page_links">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{ route('loading.create') }}"> <i class="fas fa-file-download"></i>   Saisie de Chargement  </a>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('unloading.create') }}"> <i class="fas fa-truck-loading"></i>  Déchargement Camion  </a>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('loading.index') }}"> <i class="fas fa-vote-yea"></i>  Liste des Bons des Chargements </a>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('unloading.index') }}"> <i class="fas fa-vote-yea"></i>  Liste des Bons des Déchargements </a>
                </div>
                <div class="col-md-3">
                    <a href="#"> <i class="fas fa-vote-yea"></i>  Gestion des Camions </a>
                </div>
            </div>
        </div>
    </div>
@endsection
