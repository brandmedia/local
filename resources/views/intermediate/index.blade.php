@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 offset-3">
                <a href="{{ route('intermediate.create') }}" class="btn btn-primary">Ajouté</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 offset-3">
                <p>Liste des Intermédiaire :</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 offset-3">
                <ul>
                    @foreach($intermediates as $intermediate)
                        <li>{{ $intermediate->name }} <a href="{{ route('intermediate.edit',compact('intermediate')) }}">Mettre A jour</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@stop