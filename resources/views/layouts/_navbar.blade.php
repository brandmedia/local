<!-- header --->
<header class="admin-header">
    <div class="left-part">
        <a href="#" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg"><b>Gaz </b> Control Panel </span>
        </a>
    </div>
    <div class="right-part">
        <a href="#" class="float-left to_small_sidebar" id="to_small_sidebar" data-toggle="push-menu" role="button">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </a>

        <div class="dropdown float-right">
            <a role="button" class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <img src="{{ asset('images/logo-gaz.png') }}"
                     style="width:25px;border-radius:50%;margin-left: 5px;">
                {{ auth()->user()->staff->full_name }}
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <a class="dropdown-item" href="{{ route('psw.edit') }}">mot de passe</a>
                <div class="dropdown-divider"></div>
                <a href="#"
                   onclick="event.preventDefault();
               document.getElementById('logout-form').submit();"
                   class="dropdown-item btn3 flex-c-m size13 txt11 trans-0-4 m-l-r-auto">
                    Se déconnecter
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <div class="dateete">
            @php setlocale(LC_TIME, "fr");@endphp
            {{  Carbon\Carbon::now()->formatLocalized('%d %B %Y') }}
        </div>

    </div>
</header>