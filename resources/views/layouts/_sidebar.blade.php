<section class="main-sidebar">
    <div class="container">
        <div class="user_panel">
            <div class="row">
                <div class="col-sm-12">
                    <div class="info">
                        <label>{{ auth()->user()->full_name }}</label>
                        <label class="online"> <i class="fa fa-circle text-success"></i> online </label>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start Navigation -->
        <ul class="sidebar-menu tree">
            <li class="header">Navigation</li>

            <li>
                <a href="{{ route('home') }}" class="btn bg-gray   text-left">
                    <i class="fa fa-home"></i> <span>Accueil</span>
                </a>
            </li>
            @if(auth()->user()->is_admin)
                <li>
                    <a href="{{ route('staff.index') }}" class="btn bg-gray text-left">
                        <i class="fa fa-users"></i> <span>Utilisateurs</span>
                    </a>
                </li>
            @endif
            <li>
                <a href="{{ route('provider.links') }}">
                    <i class="fas fa-truck"></i> <span> Fournisseurs</span>
                </a>
            </li>
            <li>
                <a href="{{ route('truck.index') }}">
                    <i class="fas fa-truck"></i> <span> Transport</span>
                </a>
            </li>
            <li>
                <a href="{{ route('distributeur.links') }}">
                    <i class="fas fa-truck"></i> <span> Distributeur</span>
                </a>
            </li>
            <li>
                <a href="{{ route('client.links') }}">
                    <i class="fas fa-truck"></i> <span> Client</span>
                </a>
            </li>
            <li>
                <a href="{{ route('saisie') }}">
                    <i class="fas fa-truck"></i> <span> Saisie</span>
                </a>
            </li>
            <li>
                <a href="{{ route('trade.create') }}">
                    <i class="fas fa-truck"></i> <span> Achat Vente Bouteille</span>
                </a>
            </li>
        </ul>
    </div>
</section>