@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="bon-command-fournisseur">
            <form action="{{ route("loading.store") }}" method="POST">
            @csrf
            <!-- provider -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Fournisseur :</b>
                    <select name="provider" title="Provider" id="provider" class="btn-spanen" required>
                        @foreach($providers as $provider)
                            <option value="{{ $provider->id }}">
                                {{ $provider->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!-- transporter -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Distributeur :</b>
                    <select name="truck" title="Truck" class="btn-spanen" required>
                        @foreach($trucks as $truck)
                            <option value="{{ $truck->id }}">
                                {{ $truck->registered }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <!-- products -->
                <div class="row">
                    <!-- gaz -->
                    <div class="col-md-6 offset-3">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th colspan="3" class="text-center">GAZ</th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th>Size</th>
                                <th>Quantité</th>
                                <th>PRIX</th>
                            </tr>
                            </thead>
                            <tbody style="background: #7cb3b9;">
                            @foreach($gazes as $gaze)
                                <tr style="max-width: 160px !important;">
                                    <td class="text-center"><b>{{ $gaze->bottle_size }}</b></td>
                                    <td>
                                        <input type="number" min="0"
                                               name="gaz[{{ $gaze->id }}]" value="{{ old('gaz['. $gaze->id .']') }}"
                                               data-price="{{ $gaze->lastPrice()['buy'] }}"
                                               data-tva="10"
                                               data-target="#gaz_price_{{ $gaze->id }}"
                                               class="btn-spanen prices" style="max-width: 80px !important;">
                                    </td>
                                    <td>
                                        <input type="text" id="gaz_price_{{ $gaze->id }}"
                                               class="btn-spanen total_ttc"
                                               style="max-width: 80px !important;"
                                               disabled>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($errors->has('gaz'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <span class="text-danger">{{ $errors->first('gaz') }}</span>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- Submit -->
                <div class="row mt-5">
                    <div class="col-md-6 text-left">
                        <h5 class="mode_paiement_title">Mode de Paiement </h5>
                        <table>
                            <tr>
                                <td><h5>cash </h5></td>
                                <td>
                                    <input type="number" name="cash" value="{{ old('cash') }}"
                                           placeholder="cash" class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('cash'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('cash') }}</span>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                    <div class="col-md-6">
                        <button class="btn-imprimer"><i class="fas fa-file-download"></i> Validé</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@push('scripts')
<script>
    (function () {
        function ttc() {
            var $total_ttc = 0;
            $('.total_ttc').each(function () {
                if ($(this).val().length > 0) {
                    $total_ttc = parseInt($(this).val()) + parseInt($total_ttc)
                }
            })
            $('#ttc').val($total_ttc.toFixed(2) + ' MAD')
        }

        function consign() {
            $('.consign').each(function () {
                $(this).hide();
            });
            $('.qt_consign').each(function () {
                $(this).val('');
            });
            var $provider = $('#provider').val();
            $('#consign_' + $provider).show(950);
            $('#def_' + $provider).show(950);
        }

        consign()
        ttc()
        var $body = $('body');
        $body.on('change', '#provider', function () {
            consign()
            ttc()
        })

        $body.on('change', ".prices", function () {
            var $price = $(this).attr('data-price');
            var $tva = $(this).attr('data-tva');
            var $qt = $(this).val()
            var $ht = parseInt($price * $qt)
            var $ttc = parseInt(($ht * $tva / 100) + $ht)
            var $target = $(this).attr('data-target');
            $($target).val($ttc);
            ttc()
        })
    })(jQuery)
</script>
@endpush