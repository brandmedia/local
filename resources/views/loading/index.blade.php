@extends('layouts.app')
@section('content')
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <table>
                    <thead>
                    <tr>
                        <th>Distributeur</th>
                        <th>Fournisseur</th>
                        <th>Payment</th>
                        <th>status</th>
                        <th>edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($loadings as $loading)
                        <tr>
                            <td>{{ $loading->truck->registered }}</td>
                            <td>{{ $loading->partner->name }}</td>
                            <td>{{ ($payment = $loading->payment) ? $payment->price . ' MAD' : '0 MAD'}}</td>
                            @if($loading->valid)
                                <td colspan="2">Valid</td>
                            @else
                                <td>AT</td>
                                <td>
                                    <a href="{{ route('loading.edit',compact('loading')) }}"> EDIT</a>

                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop