@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route("claim.update",compact("provider","claim")) }}" method="POST">
            @csrf
            @method("PUT")
            <table class="table table-bordered text-center table-dynamic-rows table-date-filter">
                <thead>
                <tr>
                    <th colspan="4">Paiement Créance : {{ $provider->name }}</th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th>BC n°</th>
                    <th>A term</th>
                    <th>Numéro de facture</th>
                    <th>Prix</th>
                </tr>
                </thead>
                <tbody style="background: #7cb3b9;" id="form_data">
                @foreach($claim->claimDetails as $detail)
                    <tr>
                        <td>{{ $detail->trade->bc->nbr }}</td>
                        <td>{{ $detail->term . " MAD" }}</td>
                        <td>
                            <input type="number" name="inv[{{$detail->id}}]" class="btn-spanen"
                                   placeholder="Inv" value="{{ (old('inv.' . $detail->id)) ?? $detail->inv }}" style="max-width: 200px">
                        </td>
                        <td><input type="number" name="term[{{$detail->id}}]" class="btn-spanen" value="{{ $detail->term }}"
                                   placeholder="Prix" style="max-width: 200px"></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <br>
            <div class="text-left">
                <div class="col-md-6 text-left">
                    <div class="text-left">
                        <h5 class="mode_paiement_title">Mode de Paiement </h5>
                        <table>
                            <tr>
                                <td><h5>Chéque </h5></td>
                                <td><input type="number" name="cheque"
                                           value="{{ (old('cheque')) ?? $claim->cheque->price }}"
                                           placeholder="montant" class="btn-spanen">
                                    <input type="number" name="nbr_cheque"
                                           value="{{ (old('nbr_cheque')) ?? $claim->cheque->nbr_operation }}"
                                           placeholder="numéro de chéque"
                                           class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @if($errors->has('nbr_cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('nbr_cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td><h5>Virement </h5></td>
                                <td>
                                    <input type="number" name="transfer"
                                           value="{{ (old('transfer')) ?? $claim->transfer->price }}"
                                           placeholder="virement" class="btn-spanen">
                                    <input type="number" name="nbr_operation"
                                           value="{{ (old('nbr_operation')) ?? $claim->transfer->nbr_operation }}"
                                           placeholder="virement"
                                           class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('transfer'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('transfer') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @if($errors->has('nbr_operation'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('nbr_operation') }}</span>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            <button class="btn-imprimer" id="submit"><i class="fas fa-file-download"></i> Validé</button>
        </form>


    </div>
@endsection
