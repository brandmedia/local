@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <table class="col-md-12">
                    <thead>
                    <tr>
                        <th>BC</th>
                        <th>Facture</th>
                        <th>prix</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($claims as $claim)
                        @foreach($claim->claimDetails as $detail)
                            <tr>
                                <td>{{ $detail->bc_nbr }}</td>
                                <td>{{ $detail->inv }}</td>
                                <td>{{ $detail->term }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="#"
                                   onclick="event.preventDefault();
                                           document.getElementById('{{ "destroy-claim-$claim->id" }}').submit();"
                                >Delete</a>
                                <form id="destroy-claim-{{ $claim->id }}"
                                      action="{{ route("claim.destroy",compact("claim","provider")) }}"
                                      method="POST" style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop