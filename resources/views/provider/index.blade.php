@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-5">
            @if(auth()->user()->is_admin)
                <div class="col-md-12">
                    <div class="float-right">
                        <a href="{{ route('provider.create') }}"
                           class="btn btn-success text-right"
                        >Ajouter Une nouveau Fournisseur</a>
                    </div>
                </div>
            @endif
        </div>
        <div style="max-width:600px;text-align:left">
            <table id="example" class="display dataTables_wrapper" style="width:100%;">
                <thead>
                <tr>
                    <th class="text-left">Compte</th>
                    <th class="text-left">Fournisseur</th>
                    <th class="text-left">Gérant</th>
                    <th>Chiffre d'affaire</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($providers[0]))
                    @foreach($providers as $provider)
                        <tr>
                            <td class="text-left">{{ $provider->account }}</td>
                            <td class="text-left">{{ $provider->name }}</td>
                            <td class="text-left">{{ $provider->speaker }}</td>
                            <td>{{ $provider->turnover }} MAD</td>
                            <td>
                                <a href="{{ route('provider.show',compact('provider')) }}"
                                   class="btn btn-success btn-sm"> <i class="fas fa-eye"></i> Compte de Fournisseur</a>
                            </td>
                            <td>
                                <a href="{{ route('claim.index',compact('provider')) }}"
                                   class="btn btn-success btn-sm"> <i class="fas fa-eye"></i> Liste des Paiements Créances</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4" class="text-center">Pas de Fournisseur pour Le moment Veuillez Ajouté un nouveau</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
