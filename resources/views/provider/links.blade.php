@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page_links">
            <div class="row">
                <div class="col-md-4">
                    <a href="{{ route("bc.create") }}" > <i class="fas fa-truck-moving"></i> Bon de Commande</a>
                </div>
                <div class="col-md-4">
                    <a href="{{ route('bl.create') }}" > <i class="fas fa-truck-moving"></i> Bon de Livraison</a>
                </div>
                <div class="col-md-4">
                    <a href="{{ route('provider.index') }}" ><i class="fas fa-list-ul"></i>  List des Fournisseurs  </a>
                </div>
                <div class="col-md-4">
                    <a href="{{ route('provider.create') }}" ><i class="fas fa-plus"></i>  Ajouté un nouveau Fournisseurs  </a>
                </div>
                <div class="col-md-4">
                    <a href="{{ route('intermediate.create') }}" ><i class="fas fa-plus"></i>  Ajouté un intermédiaire  </a>
                </div>
            </div>
        </div>
    </div>
@endsection
