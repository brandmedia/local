@extends('layouts.app')

@section('content')
    <div class="container page-cleint-historique">
        <div class="row">
            <div class="col-md-4">
                <table>
                    <tr class="mb-4">
                        <td style="padding: 10px 0;">Compte Fournisseur</td>
                        <td><span class="span_designed"><b>{{ $provider->account }}</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Raison Sociale</td>
                        <td><span class="span_designed"><b>{{ $provider->name }}</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Gérant</td>
                        <td><span class="span_designed"><b>{{ $provider->speaker }}</b></span></td>
                    </tr>
                    <tr>
                        <td colspan="2"><br><br><br></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Chiffre d'affaire</td>
                        <td><span class="span_designed"><b>{{ $provider->turnover }} MAD</b></span></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0;">Bénéfice</td>
                        <td><span class="span_designed"><b>?? MAD</b></span></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <table class="table table-no-border table-date-filter">
                    <tr>
                        <td>De</td>
                        <td><input type="date"></td>
                    </tr>
                    <tr>
                        <td>A</td>
                        <td>
                            <input type="date">
                            <button class="btn-search-filterr" type="button"><i class="fas fa-search"></i></button>
                        </td>
                    </tr>
                </table>
            </div>
            @isset($products["total"])
                <div class="col-md-4">
                    <div class="float-right">
                        <table class="table table-bordered" style="max-width: 250px;">
                            <thead>
                            <tr>
                                <th>QT</th>
                                <th>Retenu</th>
                                <th style="min-width: 150px">Prix</th>
                            </tr>
                            </thead>
                            <tr>
                                <td><b>3KG</b></td>
                                <td><span class="span_designed">{{ $products['3kg']['qt'] }}</span></td>
                                <td><span class="span_designed">{{ $products['3kg']['ttc'] }} MAD</span></td>
                            </tr>
                            <tr>
                                <td><b>6KG</b></td>
                                <td><span class="span_designed">{{ $products['6kg']['qt'] }}</span></td>
                                <td><span class="span_designed">{{ $products['6kg']['ttc'] }} MAD</span></td>
                            </tr>
                            <tr>
                                <td><b>12KG</b></td>
                                <td><span class="span_designed">{{ $products['12kg']['qt'] }}</span></td>
                                <td><span class="span_designed">{{ $products['12kg']['ttc'] }} MAD</span></td>
                            </tr>
                            <tr>
                                <td><b>35KG</b></td>
                                <td><span class="span_designed">{{ $products['35kg']['qt'] }}</span></td>
                                <td><span class="span_designed">{{ $products['35kg']['ttc'] }} MAD</span></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>TOTAL</b></td>
                                <td><span class="span_designed">{{ $products['total'] }} MAD</span></td>
                            </tr>
                        </table>

                    </div>

                </div>
            @endisset
        </div>
        <div class="btn btn-lg btn-solde">Solde <span>{!! $provider->sold !!}</span></div>
        <br>
        <table id="example" class="display dataTables_wrapper" style="width:100%">
            <thead>
            <tr>
                <th>Date</th>
                <th>Intermédiaire</th>
                <th>Libellé</th>
                <th>Détails</th>
                <th>QN Entre</th>
                <th>QN Sortie</th>
                <th>Débit</th>
                <th>Crédit</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($provider->trades[0]))
                @foreach($provider->trades as $trade)
                    @if($trade->bc)
                        @foreach($trade->bc->orders as $order)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($trade->created_at)->format('d / m / y') }}</td>
                                <td>{{ $trade->bc->intermediate->name }}</td>
                                <td>BC {{ $order->product->name }} n° {{ $trade->bc->nbr }}</td>
                                <td>{{ $order->product->bottle_size }}</td>
                                <td>{{ ($order->product->name == 'consigne') ? '' : $order->qt }}</td>
                                <td></td>
                                <td>{{ $order->ht }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                    @if($trade->bl)
                        @foreach($trade->bl->orders as $order)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($trade->created_at)->format('d / m / y') }}</td>
                                <td>{{ $trade->bl->intermediate->name }}</td>
                                <td>BL {{ $order->product->name }} n° {{ $trade->bl->nbr }}</td>
                                <td>{{ $order->product->bottle_size }}</td>
                                <td></td>
                                <td>{{ $order->qt }}</td>
                                <td></td>
                                <td>{{ $order->ht }}</td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="8">Pas de transaction Pour le moment</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@endsection

