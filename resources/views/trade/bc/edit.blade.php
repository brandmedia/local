@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="bon-command-fournisseur">
            <form action="{{ route('bc.update', compact('bc')) }}" method="POST">
            @csrf
                @method('PUT')
            <!-- nbr -->
                <h3 class="text-center">
                    BON DE COMMANDE NUMERO :
                    <input style="max-width: 201px;" class="btn-spanen" title="nbr"
                           value="{{ (old('nbr')) ?? $bc->nbr }}" name="nbr"
                           type="number"
                           required>
                </h3>
                <!-- provider -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Fournisseur :</b>
                    <select name="provider" title="Provider" id="provider" class="btn-spanen" required>
                        @foreach($providers as $provider)
                            <option value="{{ $provider->id }}"
                                    @if(old('provider') && $provider->id == old('provider'))
                                    selected
                                    @elseif($provider->id == $bc->trade->partner->id)
                                    selected
                                    @endif
                            >
                                {{ $provider->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!-- intermediate -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Intermédiaire :</b>
                    <select name="intermediate" title="Intermédiaire" id="intermediate" class="btn-spanen" required>
                        @foreach($intermediates as $intermediate)
                            <option value="{{ $intermediate->id }}"
                                    @if(old('intermediate') && $intermediate->id == old('intermediate'))
                                    selected
                                    @elseif($intermediate->id == $bc->trade->intermediate->id)
                                    selected
                                    @endif
                            >
                                {{ $intermediate->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!-- transporter -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Transporteur :</b>
                    <select name="transporter" title="Provider" class="btn-spanen" required>
                        @foreach($transporters as $transporter)
                            <option value="{{ $transporter->id }}"
                                    @if(old('transporter') && $transporter->id == old('transporter'))
                                    selected
                                    @elseif($transporter->id == $bc->trade->truck->id)
                                    selected
                                    @endif
                            >
                                {{ $transporter->registered }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <!-- products -->
                <div class="row">
                    <!-- gaz -->
                    <div class="col-md-6">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th colspan="3" class="text-center">GAZ</th>
                            </tr>
                            </thead>
                            <thead>

                            <tr>
                                <th>Size</th>
                                <th>Quantité</th>
                                <th>PRIX</th>
                            </tr>
                            </thead>
                            <tbody style="background: #7cb3b9;">
                            @foreach($gazes as $gaze)
                                <tr style="max-width: 160px !important;">
                                    <td class="text-center"><b>{{ $gaze->bottle_size }}</b></td>
                                    <td>
                                        <input type="number" min="0"
                                               name="gaz[{{ $gaze->id }}]" value="{{ (old('gaz['. $gaze->id .']')) ?? $gaze->getQtOrder($bc) }}"
                                               data-price="{{ $gaze->lastPrice()['buy'] }}"

                                               data-tva="10"
                                               data-target="#gaz_price_{{ $gaze->id }}"
                                               class="btn-spanen prices" style="max-width: 80px !important;">
                                    </td>
                                    <td>
                                        <input type="text" id="gaz_price_{{ $gaze->id }}"
                                               class="btn-spanen total_ttc"
                                               style="max-width: 80px !important;"
                                               disabled>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($errors->has('gaz'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <span class="text-danger">{{ $errors->first('gaz') }}</span>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- consign -->
                    @foreach($consignees as $key => $consignee)
                        <div class="col-md-6 consign" id="consign_{{ $key }}">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th colspan="3"
                                        class="text-center">{{ str_replace('_',' ',strtoupper($consignees[$key][0]->name))  }}</th>
                                </tr>
                                </thead>
                                <thead>

                                <tr>
                                    <th>Size</th>
                                    <th>Quantité</th>
                                    <th>PRIX</th>
                                </tr>
                                </thead>
                                <tbody style="background: #7cb3b9;">
                                @foreach($consignee as $consign)
                                    <tr style="max-width: 160px !important;">
                                        <td class="text-center"><b>{{ $consign->bottle_size }}</b></td>
                                        <td>
                                            <input type="number" min="0"
                                                   name="consign[{{ $consign->id }}]"
                                                   value="{{ (old('consign['. $consign->id .']')) ?? $consign->getQtOrder($bc) }}"
                                                   data-price="{{ $consign->lastPrice()['buy'] }}"
                                                   data-tva="20"
                                                   data-target="#consign_price_{{ $consign->id }}"
                                                   class="btn-spanen qt_consign prices"
                                                   style="max-width: 80px !important;">
                                        </td>
                                        <td>
                                            <input type="text" id="consign_price_{{ $consign->id }}"
                                                   class="btn-spanen qt_consign total_ttc"
                                                   style="max-width: 80px !important;"
                                                   disabled>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
                <!-- payments and validate -->
                <div class="row mt-5">
                    <div class="col-md-6 text-left">
                        <div class="text-left">
                            <h5 class="mode_paiement_title">Mode de Paiement </h5>
                            <table>
                                <tr>
                                    <td><h5>Chéque </h5></td>
                                    <td><input type="text" name="cheque" value="{{ (old('cheque')) ?? $cheque }}"
                                               placeholder="montant" class="btn-spanen">
                                        <input type="text" name="nbr_cheque"
                                               value="{{ (old('nbr_cheque')) ?? $nbr_cheque}}"
                                               placeholder="numéro de chéque"
                                               class="btn-spanen">
                                    </td>
                                </tr>
                                @if($errors->has('cheque'))
                                    <tr>
                                        <td colspan="2">
                                            <span class="text-danger">{{ $errors->first('cheque') }}</span>
                                        </td>
                                    </tr>
                                @endif
                                @if($errors->has('nbr_cheque'))
                                    <tr>
                                        <td colspan="2">
                                            <span class="text-danger">{{ $errors->first('nbr_cheque') }}</span>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td><h5>Virement </h5></td>
                                    <td>
                                        <input type="text" name="transfer" value="{{ (old('transfer')) ?? $transfer }}"
                                               placeholder="virement" class="btn-spanen">
                                        <input type="text" name="nbr_operation"
                                               value="{{ (old('nbr_operation')) ?? $nbr_operation }}"
                                               placeholder="virement"
                                               class="btn-spanen">
                                    </td>
                                </tr>
                                @if($errors->has('transfer'))
                                    <tr>
                                        <td colspan="2">
                                            <span class="text-danger">{{ $errors->first('transfer') }}</span>
                                        </td>
                                    </tr>
                                @endif
                                @if($errors->has('nbr_operation'))
                                    <tr>
                                        <td colspan="2">
                                            <span class="text-danger">{{ $errors->first('nbr_operation') }}</span>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td><h5>A terme </h5></td>
                                    <td><input type="text" name="a_term" value="{{ (old('a_term')) ?? $a_term }}"
                                               placeholder="Montant" class="btn-spanen"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    @if($errors->has('payment'))
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="text-danger">{{ $errors->first('payment') }}</span>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-6">
                        <div>
                            <table class="table-no-border" style="margin:0 0 0 auto;">
                                <tr>
                                    <td style="padding: 10px 0;"><b>TOTAL TTC</b></td>

                                    <td><input type="text" name="ttc" id="ttc"
                                               placeholder="TTC" value="{{ ' 0 MAD' }}"
                                               class="btn-spanen" disabled></td>
                                </tr>
                            </table>

                            <button class="btn-imprimer"><i class="fas fa-file-download"></i> Validé</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@push('scripts')
<script>
    (function () {
        function ttc() {
            var $total_ttc = 0;
            $('.total_ttc').each(function () {
                if ($(this).val().length > 0) {
                    $total_ttc = parseInt($(this).val()) + parseInt($total_ttc)
                }
            })
            $('#ttc').val($total_ttc.toFixed(2) + ' MAD')
        }

        function consign() {
            $('.consign').each(function () {
                $(this).hide();
            });
            var $provider = $('#provider').val();
            $('#consign_' + $provider).show(950);
            $('#def_' + $provider).show(950);
        }

        consign()
        ttc()
        var $body = $('body');
        $body.on('change', '#provider', function () {
            consign()
            $('.qt_consign').each(function () {
                $(this).val('');
            });
            ttc()
        })

        $body.on('change', ".prices", function () {
            var $price = $(this).attr('data-price');
            var $tva = $(this).attr('data-tva');
            var $qt = $(this).val()
            var $ht = parseInt($price * $qt)
            var $ttc = parseInt(($ht * $tva / 100) + $ht)
            var $target = $(this).attr('data-target');
            $($target).val($ttc);
            ttc()
        })
    })(jQuery)
</script>
@endpush