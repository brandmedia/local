@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="bon-command-fournisseur">
            <form action="{{ route('bl.update',compact('bl')) }}" method="POST">
            @csrf
                @method('PUT')
            <!-- nbr -->
                <h3 class="text-center">
                    BON DE COMMANDE NUMERO :
                    <input style="max-width: 201px;" class="btn-spanen" title="nbr"
                           value="{{ (old('nbr')) ?? $bl->nbr }}" name="nbr"
                           type="number"
                           required>
                </h3>
                <!-- provider -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Fournisseur :</b>
                    <select name="provider" title="Provider" id="provider" class="btn-spanen" required>
                        @foreach($providers as $provider)
                            <option value="{{ $provider->id }}"
                                    @if(old('provider') && $provider->id == old('provider'))
                                    selected
                                    @elseif($provider->id == $bl->trade->partner->id)
                                    selected
                                    @endif
                            >
                                {{ $provider->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!-- intermediate -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Intermédiaire :</b>
                    <select name="intermediate" title="Intermédiaire" id="intermediate" class="btn-spanen" required>
                        @foreach($intermediates as $intermediate)
                            <option value="{{ $intermediate->id }}"
                                    @if(old('intermediate') && $intermediate->id == old('intermediate'))
                                    selected
                                    @elseif($intermediate->id == $bl->trade->intermediate->id)
                                    selected
                                    @endif
                            >
                                {{ $intermediate->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!-- transporter -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Transporteur :</b>
                    <select name="transporter" title="Provider" class="btn-spanen" required>
                        @foreach($transporters as $transporter)
                            <option value="{{ $transporter->id }}"
                                    @if(old('transporter') && $transporter->id == old('transporter'))
                                    selected
                                    @elseif($transporter->id == $bl->trade->truck->id)
                                    selected
                                    @endif
                            >
                                {{ $transporter->registered }}
                            </option>
                        @endforeach
                    </select>
                </div>
                
                <!-- products -->
                <div class="row">
                    <!-- gaz -->
                    <div class="col-md-6 offset-3">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th colspan="3" class="text-center">GAZ</th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th>Size</th>
                                <th>Quantité</th>
                                <th>PRIX</th>
                            </tr>
                            </thead>
                            <tbody style="background: #7cb3b9;">
                            @foreach($gazes as $gaze)
                                <tr style="max-width: 160px !important;">
                                    <td class="text-center"><b>{{ $gaze->bottle_size }}</b></td>
                                    <td>
                                        <input type="number" min="0"
                                               name="gaz[{{ $gaze->id }}]" value="{{ (old('gaz['. $gaze->id .']')) ?? $gaze->getQtOrder($bl) }}"
                                               data-price="{{ $gaze->lastPrice()['buy'] }}"
                                               data-tva="10"
                                               data-target="#gaz_price_{{ $gaze->id }}"
                                               class="btn-spanen prices" style="max-width: 80px !important;">
                                    </td>
                                    <td>
                                        <input type="text" id="gaz_price_{{ $gaze->id }}"
                                               class="btn-spanen total_ttc"
                                               style="max-width: 80px !important;"
                                               disabled>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($errors->has('gaz'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <span class="text-danger">{{ $errors->first('gaz') }}</span>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- Submit -->
                <div class="row mt-5">
                    <div class="col-md-6 offset-6">
                        <button class="btn-imprimer"><i class="fas fa-file-download"></i> Validé</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
