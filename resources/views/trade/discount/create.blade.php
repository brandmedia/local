@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <h3>Tableaux des Remises Pour {{ $client->name }} : </h3>
        </div>
        <form action="{{ route('discount.store',compact('client')) }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered text-center">
                        <thead>
                        <tr>
                            <th colspan="3"
                                class="text-center">GAZ
                            </th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th>Size</th>
                            <th>Remise</th>
                        </tr>
                        </thead>
                        <tbody style="background: #7cb3b9;">
                        @foreach($gazes as $gaze)
                            <tr style="max-width: 160px !important;">
                                <td class="text-center"><b>{{ $gaze->bottle_size }}</b></td>
                                <td>
                                    <input type="number" min="0"
                                           name="gazes[{{ $gaze->id }}]"
                                           value="{{ (old('gazes['. $gaze->id .']')) ?? $gaze->getDiscount($client)->price }}"
                                           class="btn-spanen qt_consign prices"
                                           style="max-width: 80px !important;">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <button class="btn-imprimer" id="submit"><i class="fas fa-file-download"></i> Validé</button>
        </form>
    </div>
@stop