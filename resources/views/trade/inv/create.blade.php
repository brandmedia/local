@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('inv.store') }}" method="POST">
            @csrf
            <table class="table table-bordered text-center table-dynamic-rows table-date-filter">
                <thead>
                <tr>
                    <th colspan="2">Painement Créance</th>
                    <th class="text-right"><a href="#" id="add_input"><i class="fa fa-plus"></i></a></th>
                </tr>

                </thead>
                <tbody style="background: #7cb3b9;" id="form_data">
                <tr>

                    <td>
                        <input type="text" name="inv" class="btn-spanen"
                               placeholder="Facture">
                    </td>
                    <td>
                        <select name="provider" id="provider" title="Fournisseur" class="btn-spanen"
                                style="max-width: 200px">
                            @foreach($providers as $provider)
                                <option value="{{ $provider->id }}">{{ $provider->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td><input type="text" class="btn-spanen" name="ttc" id="ttc" placeholder="TOTAL"
                               style="max-width: 200px"></td>
                </tr>
                @if(old('bc'))
                    @foreach(old('bc') as $key => $bc)
                        <tr>
                            <td colspan="2">
                                <input type="text" name="bc[{{$key}}]" class="btn-spanen" id="bc[{{$key}}]"
                                       value="{{ $bc }}"
                                       placeholder="Numéro de Bon de Commande" style="max-width: 200px"><br>
                                @if($errors->has("bc.$key"))
                                    {{ $errors->first("bc.$key") }}
                                @endif
                            </td>
                            <td>
                                <input type="text" name="price[{{$key}}]" class="btn-spanen .input_price" id="price"
                                       placeholder="Montant HT" style="max-width: 200px">
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="2">
                            <input type="text" name="bc[]" class="btn-spanen" id="bc[]"
                                   placeholder="Numéro de Bon de Commande" style="max-width: 200px">
                        </td>
                        <td>
                            <input type="text" name="price[]" class="btn-spanen .input_price" id="price"
                                   placeholder="Montant HT" style="max-width: 200px">
                        </td>
                    </tr>
                @endif
                @if($errors->has('bc'))
                    <tr>
                        <td colspan="3">{{ $errors->first('bc') }}</td>
                    </tr>
                @endif
                @if($errors->has('price'))
                    <tr>
                        <td colspan="3">{{ $errors->first('price') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <br>
            <div class="text-left">
                <div class="col-md-6 text-left">
                    <div class="text-left">
                        <h5 class="mode_paiement_title">Mode de Paiement </h5>
                        <table>
                            <tr>
                                <td><h5>Chéque </h5></td>
                                <td><input type="number" name="cheque" value="{{ old('cheque') }}"
                                           placeholder="montant" class="btn-spanen">
                                    <input type="number" name="nbr_cheque" value="{{ old('nbr_cheque') }}"
                                           placeholder="numéro de chéque"
                                           class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @if($errors->has('nbr_cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('nbr_cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td><h5>Virement </h5></td>
                                <td>
                                    <input type="number" name="transfer" value="{{ old('transfer') }}"
                                           placeholder="virement" class="btn-spanen">
                                    <input type="number" name="nbr_operation" value="{{ old('nbr_operation') }}"
                                           placeholder="virement"
                                           class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('transfer'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('transfer') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @if($errors->has('nbr_operation'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('nbr_operation') }}</span>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            <button class="btn-imprimer" id="submit"><i class="fas fa-file-download"></i> Validé</button>
        </form>


    </div>

@endsection
@push('scripts')
<script>
    (function () {
        function line() {
            return '<tr><td colspan="2"><input type="text" name="bc[]" class="btn-spanen" id="price" placeholder="Numéro de Bon de Commande" style="max-width: 200px"></td> <td><input type="text" name="price[]" class="btn-spanen .input_price" id="price" placeholder="Montant HT" style="max-width: 200px"></td> </tr>';
        }

        $('body').on('click', '#add_input', function () {
            $("#form_data tr:last").after(line());
        })
    })(jQuery)
</script>
@endpush
