@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <h3>Tableaux des prix : </h3>
        </div>
        <form action="{{ route('price.store') }}" method="POST">
            @csrf
            <div class="row">
                @foreach($consignees as $key => $consignee)
                    <div class="col-md-6 consign" id="consign_{{ $key }}">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th colspan="3"
                                    class="text-center">{{ str_replace('_',' ',strtoupper($consignees[$key][0]->name))  }}</th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th>Size</th>
                                <th>PRIX D'ACHAT</th>
                                <th>PRIX De VENTE</th>
                            </tr>
                            </thead>
                            <tbody style="background: #7cb3b9;">
                            @foreach($consignee as $consign)
                                <tr style="max-width: 160px !important;">
                                    <td class="text-center"><b>{{ $consign->bottle_size }}</b></td>
                                    <td>
                                        <input type="number" min="0"
                                               name="consign_buy[{{ $consign->id }}]"
                                               value="{{ (old('consign_buy['. $consign->id .']')) ?? $consign->lastPrice()['buy'] }}"
                                               data-price="{{ $consign->lastPrice()['buy'] }}"
                                               data-tva="20"
                                               data-target="#consign_price_{{ $consign->id }}"
                                               class="btn-spanen qt_consign prices"
                                               style="max-width: 80px !important;">
                                    </td>
                                    <td>
                                        <input type="number" min="0"
                                               name="consign_sale[{{ $consign->id }}]"
                                               value="{{ (old('consign_sale['. $consign->id .']')) ?? $consign->lastPrice()['sale'] }}"
                                               data-price="{{ $consign->lastPrice()['buy'] }}"
                                               data-tva="20"
                                               data-target="#consign_price_{{ $consign->id }}"
                                               class="btn-spanen qt_consign prices"
                                               style="max-width: 80px !important;">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
            <button type="submit" class="text-right">Mettre à jour</button>
        </form>

    </div>
@stop