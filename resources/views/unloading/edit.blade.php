@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="bon-command-fournisseur">
            <form action="{{ route("unloading.update",compact('unloading')) }}" method="POST">
            @csrf
                @method('PUT')
            <!-- provider -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Fournisseur :</b>
                    <select name="provider" title="Provider" id="provider" class="btn-spanen" required>
                        @foreach($providers as $provider)
                            <option value="{{ $provider->id }}">
                                {{ $provider->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!-- transporter -->
                <div style="max-width:595px;margin: 0 auto 20px;" class="text-left">
                    <b>Distributeur :</b>
                    <select name="truck" title="Truck" class="btn-spanen" required>
                        @foreach($trucks as $truck)
                            <option value="{{ $truck->id }}">
                                {{ $truck->registered }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!-- products -->
                <div class="row">
                    <!-- gaz -->
                    <div class="col-md-3">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th colspan="2" class="text-center">GAZ</th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th>Size</th>
                                <th>Quantité</th>
                            </tr>
                            </thead>
                            <tbody style="background: #7cb3b9;">
                            @foreach($gazes as $gaze)
                                <tr style="max-width: 160px !important;">
                                    <td class="text-center"><b>{{ $gaze->bottle_size }}</b></td>
                                    <td>
                                        <input type="number" min="0"
                                               name="gaz[{{ $gaze->id }}]" value="{{ (old('gaz['. $gaze->id .']')) ?? $gaze->qt }}"
                                               data-price="{{ $gaze->lastPrice()['buy'] }}"
                                               data-tva="10"
                                               data-target="#gaz_price_{{ $gaze->id }}"
                                               class="btn-spanen prices" style="max-width: 80px !important;">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($errors->has('gaz'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <span class="text-danger">{{ $errors->first('gaz') }}</span>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- etranger -->
                    <div class="col-md-3">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th colspan="2" class="text-center">Etranger</th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th>Size</th>
                                <th>Quantité</th>
                            </tr>
                            </thead>
                            <tbody style="background: #7cb3b9;">
                            @foreach($etrangers as $etranger)
                                <tr style="max-width: 160px !important;">
                                    <td class="text-center"><b>{{ $etranger->bottle_size }}</b></td>
                                    <td>
                                        <input type="number" min="0"
                                               name="etranger[{{ $etranger->id }}]"
                                               value="{{ (old('etranger['. $etranger->id .']')) ?? $etranger->qt }}"
                                               data-price="{{ $etranger->lastPrice()['buy'] }}"
                                               data-tva="10"
                                               data-target="#etranger_price_{{ $etranger->id }}"
                                               class="btn-spanen prices" style="max-width: 80px !important;">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($errors->has('etranger'))
                            <div class="row">
                                <div class="col-xs-12">
                                    <span class="text-danger">{{ $errors->first('etranger') }}</span>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- consign -->
                    @foreach($consignees as $key => $consignee)
                        <div class="col-md-3 consign" id="consign_{{ $key }}">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th colspan="2"
                                        class="text-center">{{ str_replace('_',' ',strtoupper($consignees[$key][0]->name))  }}</th>
                                </tr>
                                </thead>
                                <thead>

                                <tr>
                                    <th>Size</th>
                                    <th>Quantité</th>
                                </tr>
                                </thead>
                                <tbody style="background: #7cb3b9;">
                                @foreach($consignee as $consign)
                                    <tr style="max-width: 160px !important;">
                                        <td class="text-center"><b>{{ $consign->bottle_size }}</b></td>
                                        <td>
                                            <input type="number" min="0"
                                                   name="consign[{{ $consign->id }}]"
                                                   value="{{ (old('consign['. $consign->id .']')) ?? $consign->qt }}"
                                                   data-price="{{ $consign->lastPrice()['buy'] }}"
                                                   data-tva="20"
                                                   data-target="#consign_price_{{ $consign->id }}"
                                                   class="btn-spanen qt_consign prices"
                                                   style="max-width: 80px !important;">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                <!-- consign -->
                    @foreach($defecteusees as $key => $defecteuses)
                        <div class="col-md-3 consign" id="def_{{ $key }}">
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th colspan="2"
                                        class="text-center">{{ str_replace('_',' ',strtoupper($defecteusees[$key][0]->name))  }}</th>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th>Size</th>
                                    <th>Quantité</th>
                                </tr>
                                </thead>
                                <tbody style="background: #7cb3b9;">
                                @foreach($defecteuses as $defecteuse)
                                    <tr style="max-width: 160px !important;">
                                        <td class="text-center"><b>{{ $defecteuse->bottle_size }}</b></td>
                                        <td>
                                            <input type="number" min="0"
                                                   name="defecteuse[{{ $defecteuse->id }}]"
                                                   value="{{ (old('defecteuse['. $defecteuse->id .']')) ?? $defecteuse->qt }}"
                                                   data-price="{{ $defecteuse->lastPrice()['buy'] }}"
                                                   data-tva="20"
                                                   data-target="#defecteuse_price_{{ $defecteuse->id }}"
                                                   class="btn-spanen qt_consign prices"
                                                   style="max-width: 80px !important;">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach

                </div>
                <!-- Submit -->
                <div class="row mt-5">
                    <div class="col-md-6 text-left">
                        <h5 class="mode_paiement_title">Mode de Paiement </h5>
                        <table>
                            <tr>
                                <td><h5>Chéque </h5></td>
                                <td><input type="number" name="cheque" value="{{ (old('cheque')) ?? $unloading->cheque }}"
                                           placeholder="montant" class="btn-spanen">
                                    <input type="number" name="nbr_cheque" value="{{ (old('nbr_cheque')) ?? $unloading->nbr_cheque }}"
                                           placeholder="numéro de chéque"
                                           class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @if($errors->has('nbr_cheque'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('nbr_cheque') }}</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td><h5>cash </h5></td>
                                <td>
                                    <input type="number" name="cash" value="{{ (old('cash')) ?? $unloading->cash }}"
                                           placeholder="cash" class="btn-spanen">
                                </td>
                            </tr>
                            @if($errors->has('cash'))
                                <tr>
                                    <td colspan="2">
                                        <span class="text-danger">{{ $errors->first('cash') }}</span>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                    <div class="col-md-6">
                        <button class="btn-imprimer"><i class="fas fa-file-download"></i> Validé</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@push('scripts')
<script>
    (function () {
        function ttc() {
            var $total_ttc = 0;
            $('.total_ttc').each(function () {
                if ($(this).val().length > 0) {
                    $total_ttc = parseInt($(this).val()) + parseInt($total_ttc)
                }
            })
            $('#ttc').val($total_ttc.toFixed(2) + ' MAD')
        }

        function consign() {
            $('.consign').each(function () {
                $(this).hide();
            });
            $('.qt_consign').each(function () {
                $(this).val('');
            });
            var $provider = $('#provider').val();
            $('#consign_' + $provider).show();
            $('#def_' + $provider).show();
        }

        consign()
        ttc()
        var $body = $('body');
        $body.on('change', '#provider', function () {
            consign()
            ttc()
        })

        $body.on('change', ".prices", function () {
            var $price = $(this).attr('data-price');
            var $tva = $(this).attr('data-tva');
            var $qt = $(this).val()
            var $ht = parseInt($price * $qt)
            var $ttc = parseInt(($ht * $tva / 100) + $ht)
            var $target = $(this).attr('data-target');
            $($target).val($ttc);
            ttc()
        })
    })(jQuery)
</script>
@endpush