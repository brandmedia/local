@extends('layouts.app')
@section('content')
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <table>
                    <thead>
                    <tr>
                        <th>Distributeur</th>
                        <th>Fournisseur</th>
                        <th>Payment</th>
                        <th>edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($unloadings as $unloading)
                        <tr>
                            <td>{{ $unloading->truck->registered }}</td>
                            <td>{{ $unloading->partner->name }}</td>
                            <td>{{ ($unloading->payments[0]) ? $unloading->payments()->sum('price') . ' MAD' : '0 MAD'}}</td>
                            <td>
                                <a href="{{ route('unloading.edit',compact('unloading')) }}"> EDIT</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop