@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xs-12 col-md-12">
                <div class="card">
                    <div class="card-header">Ajouté une nouvelle Personne</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('staff.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="last_name">Nom :</label>
                                        <input type="text" name="last_name" id="last_name"
                                               value="{{ old('last_name') }}" placeholder="Nom :" class="form-control"
                                               required>
                                        @if($errors->has('last_name'))
                                            <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">Prénom</label>
                                        <input type="text" name="first_name" id="first_name"
                                               value="{{ old('first_name') }}" placeholder="Prénom"
                                               class="form-control" required>
                                        @if($errors->has('first_name'))
                                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Portable :</label>
                                        <input type="tel" name="mobile" id="mobile" value="{{ old('mobile') }}"
                                               placeholder="Portable" class="form-control">
                                        @if($errors->has('mobile'))
                                            <span class="text-danger">{{ $errors->first('mobile') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">N° de la Carte CIN</label>
                                        <input type="text" name="cin" id="cin" value="{{ old('mobile') }}"
                                               placeholder="N° de la Carte CIN" class="form-control">
                                        @if($errors->has('cin'))
                                            <span class="text-danger">{{ $errors->first('cin') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="category">Catégorie :</label>
                                        <select name="category" id="category" class="form-control" required>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"
                                                        {{ (old('category') == $category->id) ? 'selected' : '' }}>
                                                    {{ $category->category }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('category'))
                                            <span class="text-danger">{{ $errors->first('category') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 account">
                                    <div class="form-group">
                                        <label for="name">Nom d'utilisateur</label>
                                        <input type="text" name="name" id="name" value="{{ old('name') }}"
                                               placeholder="Nom d'utilisateur" class="form-control" >
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 account" id="account">
                                    <div class="form-group">
                                        <label for="password">Mot de passe :</label>
                                        <input type="password" name="password" id="password" placeholder="Mot de passe"
                                               class="form-control" >
                                        @if($errors->has('password'))
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 account">
                                    <div class="form-group">
                                        <label for="password_confirmation">confirmation Mot de passe :</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation"
                                               placeholder="confirmation Mot de passe" class="form-control">
                                        @if($errors->has('password_confirmation'))
                                            <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" name="Créer" id="Create" value="Créer"
                                           class="btn btn-primary float-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    (function () {
        function selected() {
            var category = parseInt($('#category').val())
            if (category === 2 || category === 4) {
                $('.account').show()
            } else {
                $('.account').hide()
            }
        }

        selected()
        $('body').on('change', '#category', function () {
            selected()
        })
    })(jQuery)
</script>
@endpush
