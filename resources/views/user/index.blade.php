@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="float-right">
                    <a href="{{ route('staff.create') }}"
                       class="btn btn-success text-right"
                    >Ajouter Une nouvelle personne</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nom et Prénom</th>
                        <th>category</th>
                        <th>Mobile</th>
                        <th>CIN</th>
                        <th class="text-center" colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($staffs as $staff)
                        <tr>
                            <td class="text-left">{{ $staff->full_name }}</td>
                            <td>{{ $staff->category->category }}</td>
                            <td>{{ $staff->mobile }}</td>
                            <td>{{ $staff->cin }}</td>
                        
                            @if($staff->category_id !== 1)
                                @if($staff->trashed())
                                    <td colspan="2">
                                        <a href="#" class="btn btn-warning"
                                           onclick="event.preventDefault();
                                                   document.getElementById('restore-user-{{ $staff->id }}').submit();">
                                            {{ __('Réstorer') }}
                                        </a>

                                        <form id="restore-user-{{ $staff->id }}"
                                              action="{{ route('staff.restore',compact('staff')) }}"
                                              method="POST" style="display: none;">
                                            @csrf
                                            @method('PATCH')
                                        </form>
                                    </td>
                                @else
                                    <td>
                                        <a href="{{ route('staff.edit',compact('staff')) }}"
                                           class="btn btn-success">Mettre à jour</a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-danger" onclick="event.preventDefault();
                                                document.getElementById('delete-user-{{ $staff->id }}').submit();">
                                            {{ __('Supprimé') }}
                                        </a>

                                        <form id="delete-user-{{ $staff->id }}"
                                              action="{{ route('staff.destroy',compact('staff')) }}"
                                              method="POST" style="display: none;">
                                            @csrf
                                            @method('delete')
                                        </form>
                                    </td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

