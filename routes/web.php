<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register'    => false]);
Route::middleware('auth')->group(function () {
    // Home
    Route::get('/', 'HomeController@index')->name('home');
    // Users
    Route::namespace('User')->group(function (){
        Route::get('psw','PswController@edit')->name('psw.edit');
        Route::put('psw','PswController@update')->name('psw.update');

        Route::resource('staff','StaffController')->except(['show']);
        Route::patch('staff/{staff}/restore', 'StaffController@restore')
            ->name('staff.restore');
    });
    // Payment
    Route::namespace('Payment')->group(function (){
        // Claim
        Route::resource('provider/{provider}/claim','ClaimController')->except(['show', "edit", "update"]);
        // Debt
        Route::resource('client/{client}/debt','DebtController')->except(['destroy', 'show']);
    });
    // Provider
    Route::namespace('Provider')->group(function (){
        Route::get('provider/links',"ProviderController@links")->name("provider.links");
        Route::resource('provider',"ProviderController")->except(['destroy']);
    });
    // Client
    Route::namespace('Client')->group(function (){
        // crud Client && searches
        Route::get('client/links',"ClientController@links")->name("client.links");
        Route::get('client/all',"ClientController@all")->name('client.all');
        Route::post('client/search/city',"ClientController@searchCity")->name('client.search.city');
        Route::post('client/search/name',"ClientController@searchName")->name('client.search.name');
        Route::resource('client',"ClientController")->except(['destroy']);
    });
    // intermediate
    Route::namespace('Intermediate')->group(function (){
        Route::resource('intermediate','IntermediateController')
            ->except(['destroy', 'show']);
    });
    // saisie
    Route::get('saisie',function (){
        return view('saisie.links');
    })->name('saisie');
    //truck
    Route::namespace('Truck')->group(function (){
        // truck
        Route::resource('truck',"TruckController")->except(['destroy']);
        // charge
        Route::resource('charge','ChargeController')->only(['create', "store"]);
        // loading
        Route::resource('loading','LoadingController')->except(['destroy', "show"]);
        // unloading
        Route::resource('unloading','UnloadingController')->except(['destroy', "show"]);
        // distributeur Links
        Route::get('distributeur', function () {
            return view('distributeur.links');
        })->name('distributeur.links');
    });
    // Trade
    Route::namespace('Trade')->group(function (){
        // BC
        Route::resource('bc','BcController')->except(['destroy']);
        // BL
        Route::resource('bl',"BlController");
        // Buy && Sale
        Route::resource('trade','TradeController')->only(['create', 'store']);
    });

    // Product
    Route::namespace('Product')->group(function (){
        Route::resource('price','PricesController')->only(['create', 'store']);
        Route::get('discount',"DiscountController@client")->name("discount.client");
        Route::resource('client/{client}/discount','DiscountController')->only(['create', 'store']);
    });



    // todo :: Debt Client, a_term
    // todo :: Claim Provider, a_term
    // todo:: Notification page
    // todo:: Factures liste imprimé recherche => 12

    // todo:: code barre 3d
    // todo :: Accounts  => 13

    // todo:: request and authorize => 14

    // todo :: GPS => 15
    // todo :: server => 16

});
